from django.shortcuts import render
import requests
import json
from django.http import JsonResponse, HttpResponse

from .calculator import *
from django.views.decorators.csrf import csrf_exempt
import math
# from .models import *
from .send_mail import sendMail
from pcbarcade.models import Personaldetails, Selection


@csrf_exempt
def pcb_price(request):
    if request.method == "POST":
        data = request.body
        usrData = json.loads(data)
        listdata = usrData
        board_type = listdata['radBoardType']
        different_design = listdata['txtPinBanNum']
        quality = listdata['hidNum']
        size = listdata['hidLength']
        layers = listdata['hidLayers']
        material = listdata['FR4Type']
        FR4_TG = listdata['hidFR4TG']
        thickness = listdata['radBoardThickness']
        min_track = listdata['radLineWeight']
        min_hole_size = listdata['radVias']
        solder_mask = listdata['radSolderColor']
        silkscreen = listdata['radFontColor']
        surface_finish = listdata['radPlatingType']
        gold_fingers = listdata['radGoldfingers']
        finished_copper = listdata['radCopperThickness']
        postal_code = listdata['pincode']
        city = listdata['city']
        length = listdata['hidLength']
        width = listdata['hidWidth']
        via_process = listdata['radSolderCover']
        try:
            url = 'http://api-partner.pcbway.com/api/Pcb/PcbQuotation/'
            payload = {
                "Country": "India",
                "CountryCode": "IN",
                "ShipType": 1,
                "Postalcode": postal_code,
                "City": city,
                "Length": float(length),
                "Width": float(width),
                "Layers": int(layers),
                "Qty": int(quality),
                "thickness": float(thickness),
                "Material": material,
                "DesignInPanel": int(different_design),
                "MinTrackSpacing": min_track,
                "MinHoleSize": float(min_hole_size),
                "SolderMask": solder_mask,
                "Silkscreen": silkscreen,
                "SurfaceFinish": surface_finish,
                "ViaProcess": via_process,
                "FinishedCopper": finished_copper,
                "FR4Tg": FR4_TG,
                "Goldfingers": gold_fingers,
            }
            headers = {'api-key': 'W293690A 8C50BE933C8644806E9E197FAE10DD74'}
            res = requests.post(url, json=payload, headers=headers)
            print(payload)
            quotationdata = res.json()
            print("--------")
            print(quotationdata)
            pricelist = quotationdata['priceList']
            Price = pricelist[0]
            Price_final = Price['Price']
            Shipping = quotationdata['Shipping']
            Weight = Shipping['Weight']
            x, y = Shipping['ShipDays'].split('-')
            ship_cost = Shipping['ShipCost']
            build_days = Price['BuildDays']

            global pcb_weight
            pcb_weight = Weight
            global pcb_days
            avg_days = (int(x) + int(y)) / 2
            pcb_days = avg_days + build_days
            global total_cost
            # total_cost = calculateTotal(Weight, Price_final)
            total_cost = total_price_inr(Price_final, float(ship_cost), Weight)
            print("Total_cost_for_pcb  " + str(total_cost))
            responseData = {
                'error': False,
                'data': {
                    'weight': Weight,
                    'total_cost': total_cost,
                }
            }

            # In order to serialize objects other than dict you must set the safe parameter to False:
            return JsonResponse(responseData, safe=False)
        except:
            responseData = {
                'error': True,
                'errorMsg': 'INTERNAL_ERROR',
                'data': {}
            }
            # In order to serialize objects other than dict you must set the safe parameter to False:
            return JsonResponse(responseData, safe=False)


##################################### JLC PCb ################################
def jlc_price(request):
    global total_cost_One
    if request.method == "POST":
        data = request.body
        usrData = json.loads(data)
        listdata = usrData
        board_type = listdata['radBoardType']
        different_design = listdata['txtPinBanNum']
        quality = listdata['hidNum']
        size = listdata['hidLength']
        layers = listdata['hidLayers']
        material = listdata['FR4Type']
        FR4_TG = listdata['hidFR4TG']
        material_detail = 0
        if material == "FR-4":
            if FR4_TG == "TG130":
                material_detail = 0
            elif FR4_TG == "TG150":
                material_detail = 1
        else:
            total_cost_One = 999999
            return


        thickness = listdata['radBoardThickness']
        min_track = listdata['radLineWeight']
        min_hole_size = listdata['radVias']
        solder_mask = listdata['radSolderColor']
        if solder_mask == 'Green':
            color = 0
        elif solder_mask == 'Red':
            color = 1
        elif solder_mask == 'Yellow':
            color = 2
        elif solder_mask == 'Blue':
            color = 3
        elif solder_mask == 'White':
            color = 4
        elif solder_mask == 'Black':
            color = 5
        else:
            color = 1
        silkscreen = listdata['radFontColor']
        surface_finish = listdata['radPlatingType']
        if surface_finish == "HASL with lead":
            surface_finish_code = 0
        elif surface_finish == "HASL lead free":
            surface_finish_code = 1
        else:
            surface_finish_code = 0

        gold_fingers = listdata['radGoldfingers']
        if gold_fingers == "Yes":
            gold_finger = 1
        else:
            gold_finger = 0
        finished_copper = listdata['radCopperThickness']
        if finished_copper == "1 oz Cu":
            copper_weight = 1
        elif finished_copper == "2 oz Cu":
            copper_weight = 2
        else:
            copper_weight = 1

        half_hole = 0
        if 'hidPlatedHalfHole' in listdata:
            half_hole = 1

        postal_code = listdata['pincode']
        city = listdata['city']
        length = listdata['hidLength']
        width = listdata['hidWidth']
        via_process = listdata['radSolderCover']
        try:
            url = 'https://api.jlcpcb.com/exterior/order/price'
            payloads = {
                "country": "IN",
                "orderType": 1,
                "pcbParam": {
                    "layer": int(layers.strip()),
                    "length": int(length.strip()),
                    "width": int(width.strip()),
                    "qty": int(quality.strip()),
                    "thickness": float(thickness.strip()),
                    "pcbColor": color,
                    "surfaceFinish": surface_finish_code,
                    "copperWeight": copper_weight,
                    "goldFinger": gold_finger,
                    "materialDetails": material_detail,
                    "panelFlag": 0,
                    "castellatedHoles": half_hole,
                    "differentDesign": int(different_design.strip())
                }
            }
            headers = {
                "Content-Type": "application/json",
                "Accept": "application/json",
                'token': 'e3d882ac3d',
            }

            res = requests.post(url, json=payloads, headers=headers)
            print(payloads)
            quotationdata = res.json()
            print("-----------")
            print(quotationdata)
            result = quotationdata['result']
            pcbCostInfo = result['pcbCostInfo']
            weight = pcbCostInfo['weight']
            totalFee = pcbCostInfo['totalFee']

            shipping = result['shipList']
            achieve_date = result['achieveDateList']
            achieve_name = achieve_date[0]['achieveName']
            str1, str2 = achieve_name.split(' ')
            if '-' in str1:
                day1, day2 = str1.split('-')
            else:
                day2 = str1
            day = shipping[0]['day']
            ship_cost = shipping[0]['cost']
            x, y, z = day.split(' ')
            min_day, max_day = x.split('-')


            global jlc_weight
            jlc_weight = weight

            global jlc_days
            avg_days = (int(min_day) + int(max_day)) / 2
            jlc_days = avg_days + int(day2)

            # total_cost_One = calculateTotalOne(weight, totalFee)
            total_cost_One = total_price_inr(totalFee, float(ship_cost), weight)
            print("Total_cost_one_for_jlc " + str(total_cost_One))
            responseData = {
                'error': False,
                'data': {
                    'weight': weight,
                    'total_cost_One': total_cost_One,
                }
            }

            # In order to serialize objects other than dict you must set the safe parameter to False:
            return JsonResponse(responseData, safe=False)
        except Exception as err:
            print("JLC Error Encountered", err)
            responseData = {
                'error': True,
                'errorMsg': 'INTERNAL_ERROR',
                'data': {}
            }

            # In order to serialize objects other than dict you must set the safe parameter to False:
            return JsonResponse(responseData, safe=False)


@csrf_exempt
def price_compare(request):
    pcb_price(request)
    jlc_price(request)
    if total_cost > total_cost_One:
        final_price = {
            'total_cost': round(total_cost_One),
            'days': f"{math.ceil(jlc_days)} Working Days",
            'try_this': 'https://api.jlcpcb.com/exterior/order/price'
        }
        return JsonResponse(final_price, safe=False)

    else:
        final_lowest_price = {
            'total_cost': round(total_cost),
            'days': f"{math.ceil(pcb_days)} Working Days",
            'try_this': 'http://api-partner.pcbway.com/api/Pcb/PcbQuotation/'
        }
        return JsonResponse(final_lowest_price, safe=False)


def save_selection(req, total, weight, order):
    data = req.body
    usrData = json.loads(data)
    listdata = usrData
    board_type = listdata['radBoardType']
    different_design = listdata['txtPinBanNum']
    quantity = listdata['hidNum']
    size = listdata['hidLength']
    layers = listdata['hidLayers']
    material = listdata['FR4Type']
    FR4_TG = listdata['hidFR4TG']
    thickness = listdata['radBoardThickness']
    min_track = listdata['radLineWeight']
    min_hole_size = listdata['radVias']
    solder_mask = listdata['radSolderColor']
    silkscreen = listdata['radFontColor']
    surface_finish = listdata['radPlatingType']
    gold_fingers = listdata['radGoldfingers']
    finished_copper = listdata['radCopperThickness']
    postal_code = listdata['pincode']
    city = listdata['city']
    length = listdata['hidLength']
    width = listdata['hidWidth']
    via_process = listdata['radSolderCover']
    castellated_holes = "No"
    other_special_request = listdata['txtPCBNote']
    userId = listdata['gerber-file-name']
    fileUrl = listdata['gerber-file-url']
    extra_pcb = True

    length = float(length)
    width = float(width)
    thickness = float(thickness)
    ship_type = "Fedx IP"
    quantity = int(quantity)
    different_design = int(different_design)

    if 'hidPlatedHalfHole' in listdata:
        castellated_holes = listdata['hidPlatedHalfHole']

    if 'radIsNotAddToBoard' in listdata:
        if listdata['radIsNotAddToBoard'] == 'no':
            extra_pcb = False
        else:
            extra_pcb = True

    user = Personaldetails.objects.get(pk=userId)
    selection = Selection()
    selection.board_type = board_type
    selection.different_design = different_design
    selection.size = f"{length} * {width}"
    selection.quality = quantity
    selection.layers = layers
    selection.material = material
    selection.FR4_TG = FR4_TG
    selection.thickness = thickness
    selection.min_track = min_track
    selection.min_hole_size = min_hole_size
    selection.solder_mask = solder_mask
    selection.silkscreen = silkscreen
    selection.surface_finish = surface_finish
    selection.gold_fingers = gold_fingers
    selection.via_process = via_process
    selection.finished_copper = finished_copper
    selection.other_special_request = other_special_request
    selection.ship_type = 1
    selection.postal_code = postal_code
    selection.city = city
    selection.length = length
    selection.width = width
    selection.extra_pcb = extra_pcb
    selection.ship_type = ship_type
    selection.castellated_holes = castellated_holes
    selection.userId = user
    selection.order_from = order
    selection.total_cost = total
    selection.site = order
    selection.save()

    sendMail(selection.pk, userId, fileUrl)

    return selection.pk


@csrf_exempt
def save_order(request):
    pcb_price(request)
    jlc_price(request)
    if total_cost > total_cost_One:
        order_id = save_selection(request, total_cost_One, jlc_weight, "JCL Order")
        final_price = {
            'total_cost': round(total_cost_One, 2),
            'days': f"{math.ceil(jlc_days)} Working Days",
            'orderId': order_id,
            'try_this': 'https://api.jlcpcb.com/exterior/order/price',
            'link': f'http://139.59.65.211/router/selection/{order_id}/'
        }

        return JsonResponse(final_price, safe=False)
    else:
        order_id = save_selection(request, total_cost, pcb_weight, "PCB Ways Order")
        final_lowest_price = {
            'total_cost': round(total_cost, 2),
            'days': f"{math.ceil(pcb_days)} Working Days",
            'orderId': order_id,
            'try_this': 'http://api-partner.pcbway.com/api/Pcb/PcbQuotation/',
            'link': f'http://139.59.65.211/router/selection/{order_id}/'
        }
        return JsonResponse(final_lowest_price, safe=False)
