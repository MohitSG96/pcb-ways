from django.urls import path 
from . views import *
urlpatterns = [
    path('deal/', pcb_price, name="get_price"),
    path('price/', price_compare),
    path('jlc/', jlc_price),
    path('order/', save_order),
]
