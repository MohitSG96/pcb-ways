from django.apps import AppConfig


class PcbarcadeConfig(AppConfig):
    name = 'pcbarcade'
