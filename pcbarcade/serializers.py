
from rest_framework import serializers
from .models import Selection,Personaldetails


class SelectionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Selection
        fields = '__all__'


# class JLCSelectionSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = JLCSelection
#         fields = ['id','country','orderType','layer','length','width','qty','thickness','pcbColor','surfaceFinish',
#                   'copperWeight','goldFinger','materialDetails','panelFlag','impedance','flyingProbe','castellatedHoles',
#                   'edgeQty','silkColor','panelByJLCPCB_X','panelByJLCPCB_Y','differentDesign','orderDetailsRemark'
#                   ]


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Personaldetails
        fields = ['id', 'full_name', 'email_address', 'full_address', 'phone_number','file']
