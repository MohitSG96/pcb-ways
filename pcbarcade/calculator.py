
def logistic(weight):
    if(weight<=0.5):
        weight=0.5
        fedex_cost=1111
        local_cost = 60*1
    elif(0.5<weight<=1):
        weight=1
        fedex_cost=1321
        local_cost = 60*1

    elif(1<weight<=1.5):
        weight=1.5
        fedex_cost=1530
        local_cost = 60*2
    elif(1.5<weight<=2):
        weight=2
        fedex_cost=1739
        local_cost = 60*2
    elif(2<weight<=2.5):
        weight=2.5
        fedex_cost=1949
        local_cost = 60*3
    elif(2.5<weight<=3):
        weight=3
        fedex_cost=2303
        local_cost = 60*3
    elif(3<weight<=3.5):
        weight=3.5
        fedex_cost=2463
        local_cost = 60*4
    elif(3.5<weight<=4):
        weight=4
        fedex_cost=2623
        local_cost = 60*4
    elif(4<weight<=4.5):
        weight=4.5
        fedex_cost=2783
        local_cost = 60*5
    elif(4.5<weight<=5):
        weight=5
        fedex_cost=2943
        local_cost = 60*5
    elif(5<weight<=5.5):
        weight=5.5
        fedex_cost=3103
        local_cost = 60*6
    elif(5.5<weight<=6):
        weight=6
        fedex_cost=3263
        local_cost = 60*6
    elif(6<weight<=6.5):
        weight=6.5
        fedex_cost=3422
        local_cost = 60*7
    elif(6.5<weight<=7):
        weight=7
        fedex_cost=3582
        local_cost = 60*5
    elif(7<weight<=7.5):
        weight=7.5
        fedex_cost=3742
        local_cost = 60*8
    elif(7.5<weight<=8):
        weight=8
        fedex_cost=3902
        local_cost = 60*8
    elif(8<weight<=8.5):
        weight=8.5
        fedex_cost=4061
        local_cost = 60*9
    elif(8.5<weight<=9):
        weight=9
        fedex_cost=4221
    elif(9<weight<=9.5):
        weight=9.5
        fedex_cost=4381
        local_cost = 60*9
    else:
        weight=10
        fedex_cost=4541
        local_cost = 60*10

    logistic_cost=fedex_cost+local_cost
    print(f"weight:{weight}")
    print(f"fedex cost:{fedex_cost}")
    print(f"local cost:{local_cost}")
    print(f"logistic cost :{logistic_cost}")

    return logistic_cost



def pretax(weight,mf_cost,convertor=72.5):
    mf_cost=mf_cost*convertor
    x=logistic(weight)
    pretaxcost=(mf_cost+x)*1.235
    print(f"manufacturing cost: {mf_cost}")
    print(f"pretaxcost:{pretaxcost}")
    return round(pretaxcost,2)

def totalcost(weight,mf_cost,convertor):
    y=pretax(weight,mf_cost)
    total_cost=round(y*1.18,2)
    print(f"total cost :{total_cost}")
    return round(total_cost,2)


def calculateTotal(weight, dollars):
    return totalcost(weight, dollars, 72.5)