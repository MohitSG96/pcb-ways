from django.apps import AppConfig


class PcbcalculatorConfig(AppConfig):
    name = 'pcbcalculator'
