from . import views


def total_price_inr(price, shipping, weight):
    total_cost = price + shipping
    cost_in_inr = total_cost * 75  # Current Price of Dollar
    total = site_margin(cost_in_inr)
    final = total + logistic(weight)
    return final


def site_margin(total_cost):
    with_margin = total_cost * 0.235
    cost = total_cost + with_margin
    cost_with_gst = tax_in(cost)
    return cost_with_gst


def tax_in(total_cost):
    with_gst = total_cost * 0.18
    cost = total_cost + with_gst
    return cost


def logistic(weight):
    cost = 70
    if weight <= 0.5:
        local_cost = cost * 1
    elif 0.5 < weight <= 1:
        local_cost = cost * 1
    elif 1 < weight <= 1.5:
        local_cost = cost * 2
    elif 1.5 < weight <= 2:
        local_cost = cost * 2
    elif 2 < weight <= 2.5:
        local_cost = cost * 3
    elif 2.5 < weight <= 3:
        local_cost = cost * 3
    elif 3 < weight <= 3.5:
        local_cost = cost * 4
    elif 3.5 < weight <= 4:
        local_cost = cost * 4
    elif 4 < weight <= 4.5:
        local_cost = cost * 5
    elif 4.5 < weight <= 5:
        local_cost = cost * 5
    elif 5 < weight <= 5.5:
        local_cost = cost * 6
    elif 5.5 < weight <= 6:
        local_cost = cost * 6
    elif 6 < weight <= 6.5:
        local_cost = cost * 7
    elif 6.5 < weight <= 7:
        local_cost = cost * 7
    elif 7 < weight <= 7.5:
        local_cost = cost * 8
    elif 7.5 < weight <= 8:
        local_cost = cost * 8
    elif 8 < weight <= 8.5:
        local_cost = cost * 9
    elif 8.5 < weight <= 9:
        local_cost = cost * 9
    elif 9 < weight <= 9.5:
        local_cost = cost * 10
    else:
        local_cost = cost * 10

    # print(f"weight:{weight}")
    # print(f"fedex cost:{fedex_cost}")
    # print(f"local cost:{local_cost}")
    # print(f"logistic cost :{logistic_cost}")

    return local_cost

# a=float(input("Enter weight(Kg):"))
# b=float(input("Enter manufacturing cost in dollars:"))
# c=float(input("Enter convertor USD/INR:"))
# x=totalcost(a,b,c)
# print(x)
