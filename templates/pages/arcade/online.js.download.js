﻿// JavaScript Document


var online = {

    PageType: 1, //1pcb 2stencil 3:smt
    CloseSelectNumDiv: function () {
        $('#boardnumber').hide();
        online.CheckSolderCover();
    },

    CalcOrderType: function (width, height, num) {
        if (num > 50 || (width * height * num / 1000000) > 1) {
            return "small batch";
        }
        return "prototype";
    },
    CalcSquare: function (width, length, num) {
        return (parseFloat(width) * parseFloat(length) * parseInt(num) / 1000000.0);
    },

    SetColor: function (obj) {


        if ($(obj).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsSolderColor").removeClass("active");

        $(obj).addClass("active");

        $("input[name='radSolderColor']").val($(obj).attr("data"));


        var selVal = $(obj).attr("data");


        if (selVal == "None") {

            $(".clsFontColor[data='Black']").removeClass("unclickbtn");
            $(".clsFontColor[data='White']").removeClass("unclickbtn");


            //$("input[name='radFontColor'][value='Black']").attr("disabled", false);
            //$("input[name='radFontColor'][value='White']").attr("disabled", false);
        } else if (selVal == "Green") {

            $(".clsFontColor[data='Black']").removeClass("unclickbtn");
            $(".clsFontColor[data='White']").removeClass("unclickbtn");

        }
        else if (selVal == "White") {

            $(".clsFontColor[data='Black']").removeClass("unclickbtn");


            // $("input[name='radFontColor'][value='Black']").attr("disabled", false);
            if ($("input[name=radFontColor]").val() == "White") {
                $(".clsFontColor").removeClass("active");

                $(".clsFontColor[data='Black']").addClass("active");
                $("input[name='radFontColor']").val("Black");
            }

            $(".clsFontColor[data='White']").removeClass("active").addClass("unclickbtn");

            //   $("input[name='radFontColor'][value='White']").attr("disabled", true);
        }
        else if (selVal == "Yellow") {

            $(".clsFontColor[data='Black']").removeClass("unclickbtn");

            $(".clsFontColor[data='White']").removeClass("unclickbtn");


            //   $("input[name='radFontColor'][value='White']").attr("disabled", true);
        }
        else {

            $(".clsFontColor[data='White']").removeClass("unclickbtn");


            //$("input[name='radFontColor'][value='White']").attr("disabled", false);
            if ($("input[name=radFontColor]").val() == "Black") {

                $(".clsFontColor").removeClass("active");

                $(".clsFontColor[data='White']").addClass("active");
                $("input[name='radFontColor']").val("White");

                // $("input[name='radFontColor'][value='White']").attr("checked", true);
            }

            $(".clsFontColor[data='Black']").removeClass("active").addClass("unclickbtn");


            // $("input[name='radFontColor'][value='Black']").attr("disabled", true);
        }
    },


    SetLayers: function (obj) {


        var selVal = $("input[name=hidLayers]").val();

        // var selVal = 2;

        if (obj) {
            $(".clshidLayers").removeClass("active");

            $(obj).addClass("active");
            selVal = $(obj).attr("data");
            $("input[name=hidLayers]").val(selVal);


            if ($(obj).attr("class").indexOf("unclickbtn") > -1)
                return;
        }


        if (online.PageType == 4) {

            var radBoardThickness = $("input[name='radBoardThickness']").val();
            $(".clsBoardThickness").removeClass("unclickbtn");

            if (selVal == "1") {

                //$(".clsBoardThickness[data='0.08']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.1']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.13']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.15']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.18']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.23']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.26']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.3']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.35']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");

                if (radBoardThickness != "0.08" && radBoardThickness != "0.1" && radBoardThickness != "0.13" && radBoardThickness != "0.15") {
                    online.SetBoardThickness(".clsBoardThickness[data='0.1']");
                }
                else {
                    online.SetBoardThickness();
                }

            }
            else if (selVal == "2") {

                $(".clsBoardThickness[data='0.08']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.1']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.13']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.15']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.18']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.23']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.26']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.3']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.35']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");

                if (radBoardThickness != "0.1" && radBoardThickness != "0.13" && radBoardThickness != "0.15"
                    && radBoardThickness != "0.18" && radBoardThickness != "0.2" && radBoardThickness != "0.23" && radBoardThickness != "0.26") {
                    online.SetBoardThickness(".clsBoardThickness[data='0.13']");
                }
                else {
                    online.SetBoardThickness();
                }
            }
            else if (selVal == "4") {

                $(".clsBoardThickness[data='0.08']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.1']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.13']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.15']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.18']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.23']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.26']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.3']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.35']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
                
                
                if (radBoardThickness != "0.2" && radBoardThickness != "0.26" && radBoardThickness != "0.3") {
                    online.SetBoardThickness(".clsBoardThickness[data='0.26']");
                }
                else {
                    online.SetBoardThickness();
                }
            }
            else if (selVal == "6") {

                $(".clsBoardThickness[data='0.08']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.1']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.13']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.15']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.18']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.23']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.26']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.3']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.35']").addClass("unclickbtn");
                //$(".clsBoardThickness[data='0.4']").addClass("unclickbtn");

                if (radBoardThickness != "0.35" && radBoardThickness != "0.4") {
                    online.SetBoardThickness(".clsBoardThickness[data='0.35']");
                }
                else {
                    online.SetBoardThickness();
                }
            }
            else if (selVal == "8") {
                
                $(".clsBoardThickness[data='0.08']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.1']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.13']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.15']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.18']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.23']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.26']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.3']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.35']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.6']").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.8']").addClass("unclickbtn");

                if (radBoardThickness != "0.4") {
                    online.SetBoardThickness(".clsBoardThickness[data='0.4']");
                }
                else {
                    online.SetBoardThickness();
                }
            }


        }
        else {
            if (($("#FR4Type").val() == "Aluminum board" || $("#FR4Type").val() == "Copper") && parseInt(selVal) > 2) {
                $(".clsMaterial[data='1']").click();
            }

            if ($("#FR4Type").val() == "Aluminum board" && selVal == 2) {
                $("#divStructure").show();
            } else {
                $("#divStructure").hide();


            }

            if (selVal == "1") {
                $("#dvForLayerOne").show();
            }
            else {
                $("#dvForLayerOne").hide();

            }

            if (selVal == "6") {
                if (obj) {
                    $(".clsBoardThickness").removeClass("active").removeClass("unclickbtn");
                    $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.6']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
                    //$(".clsBoardThickness[data='2.4']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='1.6']").addClass("active");


                    $("input[name='radBoardThickness']").val("1.6");
                }


                //$("input[name='radBoardThickness'][value='0.4']").attr("disabled", true);
                //$("input[name='radBoardThickness'][value='0.6']").attr("disabled", true);
                //$("input[name='radBoardThickness'][value='2.4']").attr("disabled", true);
                //$("input[name='radBoardThickness'][value='1.6']").attr("checked", true);
            }
            else if (selVal == "4") {

                if (obj) {
                    $(".clsBoardThickness").removeClass("active").removeClass("unclickbtn");
                    $(".clsInsideThickness").removeClass("active").removeClass("unclickbtn");
                    $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='2.4']").removeClass("unclickbtn");
                    $(".clsBoardThickness[data='1.6']").addClass("active");

                    $("input[name='radBoardThickness']").val("1.6");
                }


                //$("input[name='radBoardThickness'][value='0.4']").attr("disabled", true);
                //$("input[name='radBoardThickness'][value='0.6']").attr("disabled", false);
                //$("input[name='radBoardThickness'][value='2.4']").attr("disabled", true);
                //$("input[name='radBoardThickness'][value='1.6']").attr("checked", true);
            }
            else if (selVal == "8") {

                if (obj) {
                    $(".clsBoardThickness").removeClass("active").removeClass("unclickbtn");
                    $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.6']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.8']").addClass("unclickbtn");

                    $("input[name='radBoardThickness']").val("1.6");
                }
            }
            else if (selVal == "10") {

                if (obj) {
                    $(".clsBoardThickness").removeClass("active").removeClass("unclickbtn");
                    $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.6']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.8']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='1.0']").addClass("unclickbtn");

                    $("input[name='radBoardThickness']").val("1.6");
                }
            }
            else if (selVal == "12") {

                if (obj) {
                    $(".clsBoardThickness").removeClass("active").removeClass("unclickbtn");
                    $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.6']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.8']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='1.0']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='1.2']").addClass("unclickbtn");

                    $("input[name='radBoardThickness']").val("1.6");
                }
            }
            else if (selVal == "14") {

                if (obj) {
                    $(".clsBoardThickness").removeClass("active").removeClass("unclickbtn");
                    $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.6']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.8']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='1.0']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='1.2']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='1.6']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='1.6']").addClass("unclickbtn");
                    $("input[name='radBoardThickness']").val("1.6");
                }
            }
            else {

                $(".clsBoardThickness").removeClass("unclickbtn");

                if ((selVal == "1" || selVal == "2") && $("#FR4Type").val() == "Aluminum board") {
                    $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.6']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='2.4']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='0.8']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='1.0']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='1.2']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='1.6']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='2.0']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='2.8']").addClass("unclickbtn");
                    $(".clsBoardThickness[data='3.2']").addClass("unclickbtn");
                    
                }

                //$("input[name='radBoardThickness'][value='0.4']").attr("disabled", false);
                //$("input[name='radBoardThickness'][value='0.6']").attr("disabled", false);
                //$("input[name='radBoardThickness'][value='2.4']").attr("disabled", false);

            }


            if (selVal == "1" && $("#FR4Type").val() == "Aluminum board") {
                $(".clsVias").removeClass("unclickbtn");
                $(".clsVias[data='0.15']").addClass("unclickbtn");
                $(".clsVias[data='0.2']").addClass("unclickbtn");
                $(".clsVias[data='0.25']").addClass("unclickbtn");
                $(".clsVias[data='0.3']").addClass("unclickbtn");
                if ($("input[name='radVias']").val() != "0.8" && $("input[name='radVias']").val() != "1.0") {
                    $("input[name='radVias']").val('0.8');
                    $(".clsVias").removeClass("active");
                    $(".clsVias[data='0.8']").addClass("active");
                }
            }
            else if (selVal == "2" && $("#FR4Type").val() == "Aluminum board") {
                $(".clsVias").removeClass("unclickbtn");
                // $(".clsVias[data='0.15']").addClass("active");
                // $(".clsVias[data='0.2']").addClass("unclickbtn");
                // $(".clsVias[data='0.25']").addClass("unclickbtn");
                // $(".clsVias[data='0.3']").addClass("unclickbtn");
                // $(".clsVias[data='0.8']").addClass("unclickbtn");
                if ($("input[name='radVias']").val() != "1.0") {
                    $("input[name='radVias']").val('1.0');
                    $(".clsVias").removeClass("active");
                    $(".clsVias[data='1.0']").addClass("active");
                }
            }
            else {
                $(".clsVias").removeClass("unclickbtn");
                $(".clsVias[data='0.8']").addClass("unclickbtn");
                $(".clsVias[data='1.0']").addClass("unclickbtn");
                if ($(".clsVias[data='0.3']") && $("input[name='radVias']").val() != "0.2" && $("input[name='radVias']").val() != "0.25" && $("input[name='radVias']").val() != "0.3") {
                    $("input[name='radVias']").val('0.3');
                    $(".clsVias").removeClass("active");
                    $(".clsVias[data='0.3']").addClass("active");

                }
            }

            if (selVal >= 4) {
                $("#divInsideThickness").show();
                $("#atg150").click(); if (ShowLayerOrder) { ShowLayerOrder() };

            }
            else {
                $("#divInsideThickness").hide();
                $("#atg130").click();
            }
            online.SetBoardThickness();
        }



        forLayerMaterialChange()

    },
    SetVias: function (obj) {

        if (obj && $(obj).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsVias").removeClass("active");

        var radVias = $(obj).attr("data");

        $("input[name='radVias']").val(radVias);



        $(obj).addClass("active");







        online.SetBoardThickness();


    }
    ,
    InsideThickness: function (obj) {

        if (obj && $(obj).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsInsideThickness").removeClass("active");

        var radInsideThickness = $(obj).attr("data");

        $("input[name='radInsideThickness']").val(radInsideThickness);

        $(obj).addClass("active");

    }
    ,
    SetBoardThickness: function (obj) {

        if (obj) {
            if ($(obj).attr("class").indexOf("unclickbtn") > -1)
                return;



            $(".clsBoardThickness").removeClass("active");

            $(obj).addClass("active");

            $("input[name='radBoardThickness']").val($(obj).attr("data"));
        }

        var selVal = $("input[name='radBoardThickness']").val();

        if (online.PageType == 4) {

            $(".clsSolderColor").removeClass("unclickbtn");
            $(".clsFontColor").removeClass("unclickbtn");

            $(".clsCopperThickness").addClass("unclickbtn");

            if ($("input[name='hidLayers']").val() == "1") {
                if (selVal == 0.08) {

                    $(".clsSolderColor[data='White']").addClass("unclickbtn");

                    if ($("input[name='radSolderColor']").val() == 'White') {
                        online.SetColor(".clsSolderColor[data='Yellow']");
                    }

                    $(".clsCopperThickness[data='0.5']").removeClass("unclickbtn");
                }
                else if (selVal == 0.1) {

                    $(".clsCopperThickness[data='0.5']").removeClass("unclickbtn");
                    $(".clsCopperThickness[data='1']").removeClass("unclickbtn");
                }
                else if (selVal == 0.13) {

                    $(".clsCopperThickness[data='0.5']").removeClass("unclickbtn");
                    $(".clsCopperThickness[data='1']").removeClass("unclickbtn");
                    $(".clsCopperThickness[data='2']").removeClass("unclickbtn");

                }
                else if (selVal == 0.15) {

                    $(".clsCopperThickness[data='2']").removeClass("unclickbtn");

                }

            }
            else if ($("input[name='hidLayers']").val() == "2") {
                if (selVal == 0.1) {

                    $(".clsCopperThickness[data='0.5']").removeClass("unclickbtn");

                    $(".clsSolderColor[data='White']").addClass("unclickbtn");

                    if ($("input[name='radSolderColor']").val() == 'White') {
                        online.SetColor(".clsSolderColor[data='Yellow']");
                    }

                }
                else if (selVal == 0.13 || selVal == 0.15 || selVal == 0.18) {

                    if (selVal == 0.13) {
                        $(".clsSolderColor[data='White']").addClass("unclickbtn");

                        if ($("input[name='radSolderColor']").val() == 'White') {
                            online.SetColor(".clsSolderColor[data='Yellow']");
                        }
                    }

                    $(".clsCopperThickness[data='0.5']").removeClass("unclickbtn");
                    $(".clsCopperThickness[data='1']").removeClass("unclickbtn");
                    $(".clsCopperThickness[data='1.5']").removeClass("unclickbtn");

                }
                else if (selVal == 0.2 || selVal == 0.23) {

                    $(".clsCopperThickness[data='1']").removeClass("unclickbtn");
                    $(".clsCopperThickness[data='1.5']").removeClass("unclickbtn");

                }
                else if (selVal == 0.26) {

                    $(".clsCopperThickness[data='2']").removeClass("unclickbtn");

                }

            }
            else if ($("input[name='hidLayers']").val() == "4") {
                // $(".clsCopperThickness").removeClass("unclickbtn");
                // $(".clsCopperThickness").removeClass("active");
                $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn");

                if (selVal == 0.2) {
                    $(".clsSolderColor[data='White']").addClass("unclickbtn");

                    if ($("input[name='radSolderColor']").val() == 'White') {
                        online.SetColor(".clsSolderColor[data='Yellow']");
                    }
                }
            }
            else if ($("input[name='hidLayers']").val() == "6") {
                $(".clsCopperThickness").removeClass("unclickbtn");
                $(".clsCopperThickness").addClass("unclickbtn");
                if (selVal == 0.35) {
                    $(".clsSolderColor[data='White']").addClass("unclickbtn");

                    if ($("input[name='radSolderColor']").val() == 'White') {
                        online.SetColor(".clsSolderColor[data='Yellow']");
                    }
                }

            }
            else if ($("input[name='hidLayers']").val() == "8") {
                $(".clsCopperThickness[data='0.5']").removeClass("unclickbtn");
                $(".clsCopperThickness[data='1']").removeClass("unclickbtn");
            }

            if ($(".clsCopperThickness.active").hasClass('unclickbtn')) {

                $(".clsCopperThickness").each(function () {
                    if (!$(this).hasClass('unclickbtn')) {
                        $(this).click();
                        return;
                    }
                });

            }

            online.SetColor(".clsSolderColor[data='" + $("input[name='radSolderColor']").val() + "']");
            

            return;
        }

        if (selVal == "0.2" || selVal == "0.4" || selVal == "0.6" || selVal == "0.8" || selVal == "2.4" || selVal == "2.8" || selVal == "3.2") {

            $(".clsCopperThickness").removeClass("active").removeClass("unclickbtn");

            $(".clsInsideThickness").removeClass("active").removeClass("unclickbtn");


            $("input[name='radCopperThickness']").val("1 oz Cu");
            $(".clsCopperThickness[data='1 oz Cu']").addClass("active");

            // $(".clsCopperThickness[data='2 oz Cu']").addClass("unclickbtn");

            $("input[name='radInsideThickness']").val("1");
            $(".clsInsideThickness[data='1']").addClass("active");



            //$("input[name='radCopperThickness'][value='1 oz Cu']").attr("checked", true);
            //$("input[name='radCopperThickness'][value='2 oz Cu']").attr("disabled", true);
        }
        else {

            // $(".clsCopperThickness").removeClass("unclickbtn");






            //$("input[name='radCopperThickness'][value='2 oz Cu']").attr("disabled", false);
        }
        if (selVal == "0.4" || selVal == "0.2") {

            $(".clsPlatingType").removeClass("active").removeClass("unclickbtn");

            $("input[name='radPlatingType']").val("Immersion gold");
            $(".clsPlatingType[data='Immersion gold']").addClass("active");

            $(".clsPlatingType[data='HASL with lead']").addClass("unclickbtn");
            $(".clsPlatingType[data='HASL lead free']").addClass("unclickbtn");



            $(".clsGoldPlatingType").removeClass("active").removeClass("unclickbtn");

            $("input[name='radGoldPlatingType']").val("Immersion gold");
            $(".clsGoldPlatingType[data='Immersion gold']").addClass("active");

            $(".clsGoldPlatingType[data='HASL with lead']").addClass("unclickbtn");
            $(".clsGoldPlatingType[data='HASL lead free']").addClass("unclickbtn");



            if ($("input[name='radGoldfingers']").val() == "Yes")
            {
                $(".clsPlatingType[data='OSP']").addClass("unclickbtn");
                $(".clsPlatingType[data='Hard Gold']").addClass("unclickbtn");
                $(".clsPlatingType[data='Immersion Silver']").addClass("unclickbtn");
                $(".clsPlatingType[data='None']").addClass("unclickbtn");

            }

           // $("#divGoldThickness").show();
           // $(".clsGoldThickness").removeClass("active");
           // $(".clsGoldThickness[data='1']").addClass("active");
            //$("input[name='radGoldThickness']").val("1");

            //if ($("input[name='radGoldfingers']").val() == "Yes" && $("input[name='radGoldPlatingType']").val() == "Immersion gold")
            //{
            //    $("#divGoldFingersThickness").show();
            //    $(".clsGoldFingersThickness").removeClass("active");
            //    $(".clsGoldFingersThickness[data='1']").addClass("active");
            //    $("input[name='radGoldFingersThickness']").val("1");
            //}
           
            //$("input[name='radPlatingType'][value='Immersion gold']").prop("checked", true);
            //$("input[name='radPlatingType'][value='HASL with lead']").attr("disabled", true);
            //$("input[name='radPlatingType'][value='HASL lead free']").attr("disabled", true);
        } else {
            $(".clsPlatingType").removeClass("unclickbtn");
            $(".clsGoldPlatingType").removeClass("unclickbtn");

            
            //if ($(this).attr("data") == "Immersion gold") {
            //    $("#divGoldThickness").show();
            //}
            //if ($("input[name='radPlatingType']").val() != "Immersion gold") {
            //    $(".clsGoldThickness").removeClass("active");
            //    $(".clsGoldThickness[data='1']").addClass("active");
            //    $("input[name='radGoldThickness']").val("1");
            //    $("#divGoldThickness").hide();
            //}
            //$("input[name='radPlatingType'][value='HASL with lead']").attr("disabled", false);
            //$("input[name='radPlatingType'][value='HASL lead free']").attr("disabled", false);
        }

        if (selVal == "0.2" || selVal == "0.4" || selVal == "0.6") {
            $(".liChamferedborderYes").hide();
            $("input[name='hidChamferedborder']").val("No");
            $("#aChamferedborder").text("No");
        } else {
            $(".liChamferedborderYes").show();

        }


        var selBoardThickness = $("input[name='radBoardThickness']").val();
        var selectLayer = parseInt($("input[name=hidLayers]").val());

        var lineWeight = $("input[name=radLineWeight]").val();
        var radVias = $("input[name=radVias]").val();

        if (lineWeight == "5/5mil" || lineWeight == "4/4mil" || radVias == "0.25" || radVias == "0.2") {

            if (lineWeight == "5/5mil" || lineWeight == "4/4mil" || $("input[name='radCopperThickness']").val() == "3 oz Cu" || $("input[name='radCopperThickness']").val() == "4 oz Cu") {
                $(".clsCopperThickness").removeClass("active").removeClass("unclickbtn");

        


                $("input[name='radCopperThickness']").val("1 oz Cu");
                $(".clsCopperThickness[data='1 oz Cu']").addClass("active");


            }

            if (lineWeight == "5/5mil" || lineWeight == "4/4mil" || $("input[name='radInsideThickness']").val() == "3" || $("input[name='radInsideThickness']").val() == "4") {

                $(".clsInsideThickness").removeClass("active").removeClass("unclickbtn");

                $("input[name='radInsideThickness']").val("1");
                $(".clsInsideThickness[data='1']").addClass("active");
            }



            //$(".clsCopperThickness[data='2 oz Cu']").addClass("unclickbtn");
            // $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");




            //$("input[name='radCopperThickness'][value='1 oz Cu']").attr("checked", true);
            //$("input[name='radCopperThickness'][value='2 oz Cu']").attr("disabled", true);
            //$("input[name='radCopperThickness'][value='3 oz Cu']").attr("disabled", true);
        }
        else if (selectLayer <= 2 && (selBoardThickness == "2.0" || selBoardThickness == "2.4")) {

            // $(".clsCopperThickness[data='3 oz Cu']").removeClass("unclickbtn");

            //$("input[name='radCopperThickness'][value='3 oz Cu']").attr("disabled", false);
        }
        else {

            if ((selBoardThickness == "2.0" || selBoardThickness == "1.6") && $("input[name='radCopperThickness']").val() == "2 oz Cu") {

            } else {
                $(".clsCopperThickness").removeClass("active").removeClass("unclickbtn");

                $("input[name='radCopperThickness']").val("1 oz Cu");
                $(".clsCopperThickness[data='1 oz Cu']").addClass("active");




                $(".clsInsideThickness").removeClass("active").removeClass("unclickbtn");
                $("input[name='radInsideThickness']").val("1");
                $(".clsInsideThickness[data='1']").addClass("active");
            }



            // $(".clsCopperThickness[data='2 oz Cu']").removeClass("unclickbtn");
            // $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");

            //$("input[name='radCopperThickness'][value='1 oz Cu']").attr("checked", true);
            //$("input[name='radCopperThickness'][value='2 oz Cu']").attr("disabled", false);
            //$("input[name='radCopperThickness'][value='3 oz Cu']").attr("disabled", true);
        }

        //if (lineWeight == "4/4mil" || radVias == "0.2") {
        //    $("input[name='radInsideThickness'][value='1']").attr("disabled", true);
        //    $("input[name='radInsideThickness'][value='1.5']").attr("disabled", true);
        //}
        // if (selectLayer >= 4 && selBoardThickness <= 0.8) {

            // $(".clsCopperThickness[data='2 oz Cu']").removeClass("active").addClass("unclickbtn");


            //$("input[name='radCopperThickness'][value='2 oz Cu']").attr("disabled", true);

        // } 

        if (selBoardThickness == 2.4) {

            $(".clsCopperThickness[data='2 oz Cu']").removeClass("active").removeClass("unclickbtn");

            // $("input[name='radCopperThickness'][value='2 oz Cu']").attr("disabled", false);

        }

        var fr4Type = $("#FR4Type").val();
        if (fr4Type && fr4Type.toString().indexOf("Aluminum") > -1) {
            $(".clsFontColor").removeClass("active").removeClass("unclickbtn");

            $("input[name='radFontColor']").val("Black");
            $(".clsFontColor[data='Black']").addClass("active");



            $(".clsSolderColor").removeClass("active").removeClass("unclickbtn");

            $("input[name='radSolderColor']").val("White");
            $(".clsSolderColor[data='White']").addClass("active");





            //$(".clsCopperThickness").removeClass("active").removeClass("unclickbtn");
            //$("input[name='radCopperThickness']").val("1 oz Cu");

            // $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
            // $(".clsCopperThickness[data='2 oz Cu']").addClass("unclickbtn");
            // $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");
            //$(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");




            //$("input[name='radFontColor'][value='Black']").attr("disabled", false);
            //$("input[name='radFontColor'][value='Black']").attr("checked", true);
            //$("input[name='radSolderColor'][value='White']").attr("disabled", false);
            //$("input[name='radSolderColor'][value='White']").attr("checked", true);
            //$("input[name='radCopperThickness'][value='1 oz Cu']").attr("checked", true);
            //$("input[name='radCopperThickness'][value='2 oz Cu']").attr("disabled", true);
            //$("input[name='radCopperThickness'][value='3 oz Cu']").attr("disabled", true);
            //$("input[name='radCopperThickness'][value='4 oz Cu']").attr("disabled", true);

        }

        if (lineWeight == "4/4mil" || lineWeight == "5/5mil") {

            $(".clsCopperThickness").removeClass("unclickbtn").removeClass("active");



            $("input[name='radCopperThickness']").val("1 oz Cu");
            $(".clsCopperThickness[data='1 oz Cu']").addClass("active");




            /*$(".clsCopperThickness[data='2 oz Cu']").addClass("unclickbtn");
            $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");
            $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");*/





            $(".clsInsideThickness").removeClass("active").removeClass("unclickbtn");
            $("input[name='radInsideThickness']").val("1");
            $(".clsInsideThickness[data='1']").addClass("active");


            $(".clsInsideThickness[data='2']").addClass("unclickbtn");
            $(".clsInsideThickness[data='3']").addClass("unclickbtn");
            $(".clsInsideThickness[data='4']").addClass("unclickbtn");

        }
        else if (lineWeight == "6/6mil") {

            //$(".clsCopperThickness").removeClass("unclickbtn").removeClass("active");

            if ($("input[name='radCopperThickness']").val() == "4 oz Cu") {
                $("input[name='radCopperThickness']").val("1 oz Cu");
                $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
            }


            $(".clsCopperThickness[data='2 oz Cu']").removeClass("unclickbtn");
            $(".clsCopperThickness[data='3 oz Cu']").removeClass("unclickbtn");

            // $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn").removeClass("active");


            $(".clsInsideThickness[data='2']").addClass("unclickbtn");
            $(".clsInsideThickness[data='3']").addClass("unclickbtn");
            $(".clsInsideThickness[data='4']").addClass("unclickbtn");


        }
        else {
            $(".clsCopperThickness").removeClass("unclickbtn");
            $(".clsInsideThickness").removeClass("unclickbtn");


        }



        online.CheckSolderCover();

        forLayerMaterialChange();

        SurfaceFinishChange();
    },

    CheckSolderCover: function () {

        var width = parseFloat($("#hidWidth").val());
        var length = parseFloat($("#hidLength").val());
        var num = $("#hidNum").val();
        if (online.CalcSquare(width, length, num) >= 5) {
            $(".clsSolderCover[data='Plugged vias']").removeClass("unclickbtn");
            //$("input[name='radSolderCover'][value='Plugged vias']").attr("disabled", false);
        } else {
            //$(".clsSolderCover[data='Plugged vias']").removeClass("active").addClass("unclickbtn");

            //$("input[name='radSolderCover'][value='Plugged vias']").attr("disabled", true);
        }

    },


    SetInputNum: function () {
        var width = parseFloat($("#hidWidth").val());
        var length = parseFloat($("#hidLength").val());
        if (isNaN(width) || width < 4) {
            $.messager.alert($("#JsMessage").val(), $("#JsPlsFillWidth").val(), 'show'); return false;
        }
        if (isNaN(length) || length <= 4) {
            $.messager.alert($("#JsMessage").val(), $("#JsPlsFillLen").val(), 'show'); return false;
        }
        var num = $("#hidNum").val();
        if (parseInt($("#txtSelNum").val()) > 4) {
            num = parseInt($("#txtSelNum").val());
            if (online.CalcSquare(width, length, num) < 5) {
                $.messager.alert($("#JsMessage").val(), $("#JsWhenBoradIsLarge").val(), 'show');
            }
            else {
                $("#hidNum").val(num); $("#hidNum").blur(); online.CloseSelectNumDiv();
                //online.SetDeliveryType();
            }
        } else { online.CloseSelectNumDiv(); }


    },
    CheckWidHei: function (obj) {
        if (parseFloat(obj.val()) > 500
        ) {
            $.messager.alert($("#JsMessage").val(), $("#JsMaxLenAndWidth").val(), 'show');
            obj.val("");

        }

    },
    GetBoardWidth: function () {
        var width = parseFloat($("#hidWidth").val());
        var length = parseFloat($("#hidLength").val());
        return width;
    }
	,
    GetBoardHeight: function () {
        var length = parseFloat($("#hidLength").val());
        return length;
    },
    SetPrev: function () {
        $("#step1Div").show();
        $("#step2Div").hide();
        $("#step1Div").prev().removeClass("current");
        $("#step1Div").prev().find("p").html("<p></p>");

    },
    Init: function () {

        $.extend($.fn.validatebox.defaults.rules, {
            num: {
                validator: function (value, param) {

                    return !isNaN(value) && value > 0;
                },
                message: $("#JsNumBiggerThanZero").val()
            }
        });


        if (!online.selectNumHtmlIsAdd && $("#boardnumber").size() == 0) {

            $(online.selectNumHtml).hide().insertAfter($("#hidNum")); online.electNumHtmlIsAdd = true;

        }
        $(".clsSolderColor").click(function () { online.SetColor(this); });
        //$(":radio[name='radSolderColor']").click(function () { online.SetColor(); });
        //$(":radio[name='hidLayers']").click(function () { online.SetLayers(); });

        $(".clshidLayers").click(function () { online.SetLayers(this); });

        $(".clsInsideThickness").click(function () { online.InsideThickness(this); });

        //
        online.SetLayers(null);

        //$(":radio[name='radBoardThickness']").click(function () { online.SetBoardThickness(); });


        $(".clsBoardThickness").click(function () { online.SetBoardThickness(this); });


        //  $(":radio[name='radVias']").click(function () { online.SetVias(); });

        $(".clsVias").click(function () { online.SetVias(this); });

        $(":radio[name='countNumer']").click(function () { $("#hidNum").val($(this).val()); $("#txtSelNum").val(""); online.CloseSelectNumDiv(); });

        $("#onlineForm").submit(function () {
            if ($("#onlineForm").form('validate')) {
                if ($("#selExpressName").val() == "") { $.messager.alert($("#JsTip").val(), $("#JsPlsSelShipType").val(), 'show'); return false; } return true;
            };
            return false;
        });

        $("#hidNum").click(function () { $("#boardnumber").show(); });

        $("#hidLength,#hidWidth").change(function () { online.CheckWidHei($(this)); });
        //$("input[name='radSolderCover'][value='Plugged vias']").attr("disabled", true);


        //$(".clsSolderCover[data='Plugged vias']").removeClass("active").addClass("unclickbtn");

        //  $(".clsFontColor[data='Black']").addClass("unclickbtn");

    }
}




$(document).ready(function () {

    $.extend($.fn.validatebox.defaults.rules, {
        //customfirst: {
        //    validator: function (value, param) {

        //        var val = $(":radio[name=radBoardType][checked]").val();
        //        if (val == "Single PCB")
        //            return true;
        //        else
        //            return value == "--" ? false : true;


        //    },
        //    message: 'Field do not match.'
        //},
        XoutValidate: {
            validator: function (value, param) {

                var val = $("#radBoardType").val();
                if (val == "Single PCB")
                    return true;
                else {
                    var val = $(":radio[name=radAcceptCrossed]:checked").val();
                    if (!val)
                        return false;
                    else
                        return true;
                }
            },
            message: $("#JsXoutValidate").val()
        },
        AcceptHASLUpValidate: {
            validator: function (value, param) {

                //debugger;
                var val = $(":radio[name=radAcceptHASLUp]:checked").val();
                if (!val)
                    return false;
                else
                    return true;

            },
            message: $("#JsAcceptHASLUpValidate").val()
        },
        IsSMTRelatedPCB: {
            validator: function (value, param) {


                var val = $(":radio[name=radPCBRelation]:checked").val();
                if (!val)
                    return false;
                else
                    return true;

            },
            message: $("#JsIsSMTRelatedPCB").val()
        }

    });




    //$("#txtPinBanNum").focus(function () {
    //    $(this).next().next().mouseover();
    //}).blur(function () {
    //    $(this).next().next().mouseout();
    //});
    //debugger;


    $("#chkPCBOptions").click(function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(".clsPCBOptions").hide();

        } else {
            $(this).addClass("active");
            $(".clsPCBOptions").show();
        }
    })

    $("#chkSMTOptions").click(function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(".clsSMTOptions").hide();

        } else {
            $(this).addClass("active");
            $(".clsSMTOptions").show();
        }
    })

    $(".clsPlatedHalfHole").click(function () {
        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsPlatedHalfHole").removeClass("active");

        $(this).addClass("active");

        $("input[name='hidPlatedHalfHole']").val($(this).attr("data"));
    })

    $(".clsSidePlating").click(function () {
        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsSidePlating").removeClass("active");

        $(this).addClass("active");

        $("input[name='hidSidePlating']").val($(this).attr("data"));
    })

    $(".clsCustomStackup").click(function () {
        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsCustomStackup").removeClass("active");

        $(this).addClass("active");

        $("input[name='hidCustomStackup']").val($(this).attr("data"));
    })

    $(".clsImpedanceControl").click(function () {
        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsImpedanceControl").removeClass("active");

        $(this).addClass("active");

        $("input[name='hidImpedanceControl']").val($(this).attr("data"));
    })

    $(".clsCountersink").click(function () {
        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsCountersink").removeClass("active");

        $(this).addClass("active");

        $("input[name='hidCountersink']").val($(this).attr("data"));
    })

    $("#dvmauirowmain").click(function (e) {
        

        if ($("#dvmauiowright:visible").size() > 0) {


            if (e.target.className.indexOf('clsIgnore') > -1 || e.target.id == "lblChkSMD" || e.target.id == "lblChkSMT" || e.target.id == "lblChkPCB") {
                return true;
            }

            Step1();
        }
       
    });


    $("input[name='chkSMDStencil']").click(function (e) {

        if (e) {
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            else {
                e.cancelBubble = true;
            }
        }



        if (this.checked) {
            $(this).parent().parent().next().show();
            $("input[name='chkSMT']").parent().parent().next().hide();
            $("input[name='chkSMT']").removeAttr("checked");

        } else {
            $(this).parent().parent().next().hide();
            // $("input[name='chkSMT']").parent().parent().next().show();
        }



        if ($("#dvmauiowright:visible").size() > 0) {

            Step1();
        }

    });



    $("input[name='chkSMT']").click(function (e) {

        if (e) {
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            else {
                e.cancelBubble = true;
            }
        }



        if (this.checked) {
            $(this).parent().parent().next().show();
            $("input[name='chkSMDStencil']").parent().parent().next().hide();
            $("input[name='chkSMDStencil']").removeAttr("checked");

        } else {
            $(this).parent().parent().next().hide();
            // $("input[name='chkSMDStencil']").parent().parent().next().show();

        }



        if ($("#dvmauiowright:visible").size() > 0) {

            Step1();
        }

    });


    $("input[name='chkPCB']").click(function (e) {

        if (e) {
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            else {
                e.cancelBubble = true;
            }
        }



        if (this.checked) {
            $(this).parent().parent().next().show();
            //$("#radPCBRelation").click();

            $('#txtPCBNos').val('');
            $('#spanPCBNos').text('');
            // $('#chkPCB').attr('checked', 'checked');
            $('#chkPCB').prop('checked', 'checked');
            $('.clsPCBRelation').removeClass('active');
            $("#radPCBRelation").addClass('active');
            // $("#radPCBRelation").attr("checked", "checked");


        } else {
            $(this).parent().parent().next().hide();
        }

        if ($("#dvmauiowright:visible").size() > 0) {

            Step1();
        }
    })

    //SMT 

    $("#spSMTAddMore").click(function () {

        if ($(".clsBoardNum").length < 3) {
            $(this).before('<input type="text" name="txtBoardNum' + $(".clsBoardNum").length + '"  style="margin-right: 3px;float:left" class="clsBoardNum inputnum  ui2-form-col6 ui2-textfield-large ui2-textfield-single" placeholder="Estimated PCBA Qty">');

            $(".inputnum").integerinput();

        }
    })
    //
    $("textarea").click(function (e) {
        if (e) {
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            else {
                e.cancelBubble = true;
            }
        }
    })

    //
    $(".stopbubble").click(function (e) {
        if (e) {
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            else {
                e.cancelBubble = true;
            }
        }
    })

    online.PageType = $("#pagetype").val();//1 pcb  2stencil 3:SMT

    online.Init();


    //
    $(".clsBoardType").click(function () {
        $('#radBoardType').val($(this).attr('data'))
        $(".clsBoardType").removeClass('active');
        $(this).addClass('active');
        var val = $(this).attr('data');
        if (val == "Single PCB") {
            $(".dropdown-toggle").hide();
            $(".clsUnit").text($("#JSingle").val());
            $(".clsUnitPCS").text($("#JPcs").val());
            $("#boardTypeTxt").text($(".ui2-popup-menu li[data='1']").text());
            $("#boardType").val(1);
        } else if (val == "Panel PCB as design") {
            $(".dropdown-toggle").children("div").eq(0).hide();

            $(".dropdown-toggle").show();
            $(".clsUnit").text($("#JPanel").val());
            $(".clsUnitPCS").text($("#JPanel").val());

            $("#boardTypeTxt").text($(".ui2-popup-menu li[data='2']").text());
            $("#boardType").val(2);
        }
        else if (val == "In Panel") {

            $(".clsUnit").text($("#JPanel").val());
            $(".clsUnitPCS").text($("#JPanel").val());

        }
        else if (val == "Panel PCB by PCBWay") {

            $(".dropdown-toggle").children("div").eq(0).show();

            $(".dropdown-toggle").show();
            $(".clsUnit").text($("#JSingle").val());
            $(".clsUnitPCS").text($("#JPcs").val());
            $("#boardTypeTxt").text($(".ui2-popup-menu li[data='2']").text());
            $("#boardType").val(2);
        }

    });


    //Min Track/Spacing
    $(".clsLineWeight").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsLineWeight").removeClass("active");

        $(this).addClass("active");

        // 选了4/4mil 或 5/5mil  不能选 2 oz Cu  3 oz Cu   4 oz Cu
        //if ($(this).attr("data") == "4/4mil" || $(this).attr("data") == "5/5mil") {
        //    $(".clsCopperThickness[data='2 oz Cu']").addClass("unclickbtn");
        //    $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");
        //    $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");
        //    // 选中第一个
        //    $(".clsCopperThickness").removeClass("active");
        //    $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
        //    $("input[name='radCopperThickness']").val('1 oz Cu');



        //    $(".clsInsideThickness[data='2']").addClass("unclickbtn");
        //    $(".clsInsideThickness[data='3']").addClass("unclickbtn");
        //    $(".clsInsideThickness[data='4']").addClass("unclickbtn");
        //    // 选中第一个
        //    $(".clsInsideThickness").removeClass("active");
        //    $(".clsInsideThickness[data='1']").addClass("active");
        //    $("input[name='radInsideThickness']").val('1');

        //}
        //else if ($(this).attr("data") == "6/6mil") {
        //    $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");
        //    // 选中第一个

        //    if ($("input[name='radCopperThickness']").val() == '4 oz Cu') {
        //        $(".clsCopperThickness").removeClass("active");
        //        $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
        //        $("input[name='radCopperThickness']").val('1 oz Cu');
        //    }
        //}
        //else {
        //    $(".clsCopperThickness[data='2 oz Cu']").removeClass("unclickbtn");
        //    $(".clsCopperThickness[data='3 oz Cu']").removeClass("unclickbtn");
        //    $(".clsCopperThickness[data='4 oz Cu']").removeClass("unclickbtn");


        //    $(".clsInsideThickness[data='2']").removeClass("unclickbtn");
        //    $(".clsInsideThickness[data='3']").removeClass("unclickbtn");
        //    $(".clsInsideThickness[data='4']").removeClass("unclickbtn");
        //}

        $("input[name='radLineWeight']").val($(this).attr("data"));

        forLayerMaterialChange()
    })

    $(".dvBtnSelAuNiPd").click(function () {
        $("#iptENEPIGType").val(1);

        SelectAuNiPdOption()
    })

    $(".dvBtnSelAuNiPdSencond").click(function () {
        $("#iptENEPIGType").val(2);
        SelectAuNiPdOption()
    })


    //Silkscreen
    $(".clsFontColor").click(function () {


        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsFontColor").removeClass("active");

        $(this).addClass("active");

        $("input[name='radFontColor']").val($(this).attr("data"));


        if ($(this).attr("data") == "None") {
            if ($("#radIsNotAddToBoard") && $("#radIsNotAddToBoard")[0])
                $("#radIsNotAddToBoard")[0].checked = true;
        }


    })

    $(".dvBtnSelAuNi").click(function () {
        $("#iptHardGoldType").val(1);

        SelectAuNiOption()
    })

    $(".dvBtnSelAuNiSencond").click(function () {
        $("#iptHardGoldType").val(2);
        SelectAuNiOption()
    })
    

    $("#btnHardGold").click(function () {

        var text = $(".clsHardGold.active").attr("data");

        var iptAUVal = "";
        var iptNIVal = "";

        if (text == "customValHardGold") {
         
            iptAUVal = $("#iptAUVal").val();
            iptNIVal = $("#iptNIVal").val();
            if ((/(^[1-9]\d*$)/).test(iptAUVal) == false) 
            {
                alert($("#JProductTip1").val()); 
                return;
            }

            if ((/(^[1-9]\d*$)/).test(iptNIVal) == false)
            {
                alert($("#JProductTip2").val());
                return;

            }
           
        } else {

            iptAUVal = text.split(',')[0];
            iptNIVal = text.split(',')[1];
        }


        //$(".clsiptGoldThickness").val(iptAUVal);
        //$(".clsiptNiGoldThickness").val(iptNIVal);


        $('#dvHardGold').window('open'); // open a window

    

        // $(".spanNiThickness").text('AU:' + iptAUVal + 'U"/Ni:' + iptNIVal + 'U"');

        if ($("input[name='radGoldfingers']").val() == "Yes" && $("input[name='radGoldPlatingType']").val() == "Hard Gold") {
          
            $("#divNiGoldThickness").show();
        } else {
            $("#divNiGoldThickness").hide();
        }
        


        if ($("input[name='radPlatingType']").val() == "Hard Gold") {
            $("#divNiSecondGoldThickness").show();
        } else {
            $("#divNiSecondGoldThickness").hide();
        }



        if ($("#divNiGoldThickness").is(":visible") == true) {
            $("#aNiGoldThickness").html('Au:' + iptAUVal + 'U"/Ni:' + iptNIVal + 'U"<i></i>');
            $("input[name='radAUGoldThickness']").val(iptAUVal);
            $("input[name='radNiGoldThickness']").val(iptNIVal);

        }

        if ($("#divNiSecondGoldThickness").is(":visible") == true) {
            $("#aNiSecondGoldThickness").html('Au:' + iptAUVal + 'U"/Ni:' + iptNIVal + 'U"<i></i>');
            $("input[name='radSendAUGoldThickness']").val(iptAUVal);
            $("input[name='radSendNiGoldThickness']").val(iptNIVal);
        }

        if ($("#iptHardGoldType").val() == 2) {
           $("#divNiGoldThickness").show();
        } else {
           $("#divNiSecondGoldThickness").hide();
         
        }

        Step1();

    })


    $("#btnENEPIG").click(function () {

        var text = $(".clsENEPIG.active").attr("data");

        var iptAUVal = "";
        var iptNIVal = "";
        var iptPdVal = "";


        if (text == "customValHardGold") {

            iptAUVal = $("#iptPdAUVal").val();
            iptNIVal = $("#iptPdNIVal").val();
            iptPdVal = $("#iptPdPdVal").val();


            if ((/(^[1-9]\d*$)/).test(iptAUVal) == false) {
                alert("Au 2-4 U\"");
                return;
            }

            //if ((/(^[1-9]\d*$)/).test(iptNIVal) == false) {
            //    alert($("#JProductTip2").val());
            //    return;

            //}

            if ((/(^[1-9]\d*$)/).test(iptPdVal) == false) {
                alert("Pd 2-6 U\"");
                return;

            }



            if (iptPdVal < 2 || iptPdVal > 6) {
                alert("Pd 2-6 U\"");
                return;

            }

            if (iptAUVal < 2 || iptAUVal > 4) {
                alert("Au 2-4 U\"");
                return;

            }

        } else {

            iptNIVal = text.split(',')[0];
            iptPdVal = text.split(',')[1];
            iptAUVal = text.split(',')[2];


        }


        //$(".clsiptGoldThickness").val(iptAUVal);
        //$(".clsiptNiGoldThickness").val(iptNIVal);


        $('#dvENEPIG').window('close'); // open a window



        //$(".spanNiThickness").text('AU:' + iptAUVal + 'U"/Ni:' + iptNIVal + 'U"');

        if ($("input[name='radGoldfingers']").val() == "Yes" && $("input[name='radGoldPlatingType']").val() == "ENEPIG") {
            $("#divPdGoldThickness").show();
        } else {
            $("#divPdGoldThickness").hide();
        }



        if ($("input[name='radPlatingType']").val() == "ENEPIG") {
            $("#divPdSecondGoldThickness").show();
        } else {
            $("#divPdSecondGoldThickness").hide();
        }



        if ($("#divPdGoldThickness").is(":visible") == true) {
            $("#aPdGoldThickness").html('Ni:' + iptNIVal + 'U"&nbsp;Pd:' + iptPdVal + 'U"&nbsp;Au:' + iptAUVal + 'U"<i></i>');
            $("input[name='radPdAUGoldThickness']").val(iptAUVal);
            $("input[name='radPdNiGoldThickness']").val(iptNIVal);
            $("input[name='radPdPdGoldThickness']").val(iptPdVal);

        }

        if ($("#divPdSecondGoldThickness").is(":visible") == true) {

            $("#aPdSecondGoldThickness").html('Ni:' + iptNIVal + 'U"&nbsp;Pd:' + iptPdVal + 'U"&nbsp;Au:' + iptAUVal + 'U"<i></i>');
            $("input[name='radSendPdAUGoldThickness']").val(iptAUVal);
            $("input[name='radSendPdNiGoldThickness']").val(iptNIVal);
            $("input[name='radSendPdPdGoldThickness']").val(iptPdVal);
        }




        Step1();

    })

    //Surface Finish:
    $(".clsPlatingType").click(function () {
        //debugger;
        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        // var oldradPlatingType = $("input[name='radPlatingType']").val();


        // if (oldradPlatingType != "Hard Gold")
        // {
        //    $(".spanNiThickness").text('AU:5U"/Ni:120U"');
        //    $("input[name='radSendAUGoldThickness']").val(5);
        //    $("input[name='radSendNiGoldThickness']").val(120);
        // }

        $(".clsPlatingType").removeClass("active");

        $(this).addClass("active");

        $("input[name='radPlatingType']").val($(this).attr("data"));

        if ($(this).attr("data") == "Immersion gold" || $(this).attr("data") == "Immersion gold (ENIG)") {
            $("#divGoldThickness").show();
        }
        else {
           // $(".clsGoldThickness").removeClass("active");
           // $(".clsGoldThickness[data='1']").addClass("active");
           // $("input[name='radGoldThickness']").val("1");
            $("#divGoldThickness").hide();


          //  $(".clsGoldFingersThickness").removeClass("active");
           // $(".clsGoldFingersThickness[data='1']").addClass("active");
          //  $("input[name='radGoldFingersThickness']").val("1");
        //$("#divGoldFingersThickness").hide();

        }
      
        SurfaceFinishChange(2);

     

        // 未登陆不显示升级选项
        if ($("#logFlag").val() == 0) {
            return;
        }

        if ($(this).attr("data") == "HASL with lead" || $(this).attr("data") == "HASL lead free") {
            if (getCookie("logFlag") == 1) {
                $(".custom-weight-show").show();
            }
        }
        else {
            $(".custom-weight-show").hide();

        }


    })

    //clsGoldThickness:
    $(".clsGoldThickness").click(function () {
        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsGoldThickness").removeClass("active");

        $(this).addClass("active");

        $("input[name='radGoldThickness']").val($(this).attr("data"));


        if ($("input[name='radGoldfingers']").val() == "Yes" && $("input[name='radGoldPlatingType']").val() == "Immersion gold") {


            $(".clsGoldFingersThickness").removeClass("active");

            $(".clsGoldFingersThickness[data=" + $(this).attr("data") + "]").addClass("active");

            $("input[name='radGoldFingersThickness']").val($(this).attr("data"));


        }

    })

    //clsGoldFingersThickness:
    $(".clsGoldFingersThickness").click(function () {
        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsGoldFingersThickness").removeClass("active");

        $(this).addClass("active");

        $("input[name='radGoldFingersThickness']").val($(this).attr("data"));

     

        if ($("input[name='radPlatingType']").val() == "Immersion gold") {


            $(".clsGoldThickness").removeClass("active").addClass("unclickbtn");

            $(".clsGoldThickness[data=" + $(this).attr("data") + "]").removeClass("unclickbtn").addClass("active");

            $("input[name='radGoldThickness']").val($(this).attr("data"));


        }
    })

    $(".clsHardGold").click(function () {
        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

       

        $(".clsHardGold").removeClass("active");
        $(this).addClass("active");
  
      

    })


    $(".clsENEPIG").click(function () {
        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }



        $(".clsENEPIG").removeClass("active");
        $(this).addClass("active");



    })

    $(".clsGoldPlatingType").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsGoldPlatingType").removeClass("active");
        $(this).addClass("active");
        $("input[name='radGoldPlatingType']").val($(this).attr("data"));


        $(".clsPlatingType").removeClass("active").addClass("unclickbtn");
        //$(".clsGoldThickness").removeClass("active");
        //$(".clsGoldThickness[data='1']").addClass("active");
        //$("input[name='radGoldThickness']").val("1");
       $("#divGoldThickness").hide();
        $("#divGoldFingersThickness").hide();

        if ($(this).attr("data") == "HASL with lead") {
            $(".clsPlatingType[data='HASL with lead']").removeClass("unclickbtn").addClass("active");
            $("input[name='radPlatingType']").val("HASL with lead");
        }
        else if ($(this).attr("data") == "HASL lead free") {
            $(".clsPlatingType[data='HASL lead free']").removeClass("unclickbtn").addClass("active");
            $("input[name='radPlatingType']").val("HASL lead free");
        }
        else if ($(this).attr("data") == "Immersion gold") {
            $(".clsPlatingType[data='Immersion gold']").removeClass("unclickbtn").addClass("active");
            $("input[name='radPlatingType']").val("Immersion gold");

       

           


          // $("#divGoldThickness").show();

           // var radGoldThickness = $("input[name='radGoldThickness']").val();
            
           // $(".clsGoldFingersThickness").removeClass("active");
          //  $(".clsGoldFingersThickness[data=" + radGoldThickness + "]").addClass("active");
          //  $("input[name='radGoldFingersThickness']").val(radGoldThickness);
          //  $("#divGoldFingersThickness").show();


        }
        else if ($(this).attr("data") == "Immersion Silver") {
            $(".clsPlatingType[data='Immersion Silver']").removeClass("unclickbtn").addClass("active");
            $("input[name='radPlatingType']").val("Immersion Silver");
        }
        else if ($(this).attr("data") == "ENEPIG") {
            $(".clsPlatingType[data='ENEPIG']").removeClass("unclickbtn").addClass("active");
            $("input[name='radPlatingType']").val("ENEPIG");
        }
        else if ($(this).attr("data") == "OSP") {
            $(".clsPlatingType[data='OSP']").removeClass("unclickbtn").addClass("active");
            $("input[name='radPlatingType']").val("OSP");
        }
        else if ($(this).attr("data") == "Hard Gold") {
           
          
            $(".clsPlatingType").removeClass("unclickbtn");
            //$(".clsPlatingType[data='Hard Gold']").addClass("unclickbtn");
            $(".clsPlatingType[data='None']").addClass("unclickbtn");

            if ($("input[name='radBoardThickness']").val() == "0.4" || $("input[name='radBoardThickness']").val() == "0.2") {
                $(".clsPlatingType[data='Immersion gold']").addClass("active");
                $("input[name='radPlatingType']").val("Immersion gold");
                $("#divGoldThickness").show();







                $(".clsPlatingType[data='HASL with lead']").addClass("unclickbtn");
                $(".clsPlatingType[data='HASL lead free']").addClass("unclickbtn");

            }
            else {
                $(".clsPlatingType[data='HASL with lead']").addClass("active");
                $("input[name='radPlatingType']").val("HASL with lead");
            }



 

        }
        else if ($(this).attr("data") == "None") {
            $(".clsPlatingType[data='None']").removeClass("unclickbtn").addClass("active");
            $("input[name='radPlatingType']").val("None");
        }



        if ($("input[name='radPlatingType']").val() == "HASL with lead" || $("input[name='radPlatingType']").val() == "HASL lead free") {
            if (getCookie("logFlag") == 1) {
                $(".custom-weight-show").show();
            }
        }
        else {
            $(".custom-weight-show").hide();

        }

        
        SurfaceFinishChange();
    })

    //Finished Coppe
    $(".clsCopperThickness").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsCopperThickness").removeClass("active");

        $(this).addClass("active");
        if ($(this).attr("data") == "2 oz Cu") {
            // 选中第一个
            $(".clsLineWeight").removeClass("active");
            $(".clsLineWeight[data='8/8mil']").addClass("active");
            $(".input[name='radLineWeight']").val('8/8mil');
            $("input[name='radLineWeight']").val('8/8mil');



            $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");
            $(".clsCopperThickness[data='2 oz Cu']").addClass("unclickbtn");
            $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");


            $(".clsInsideThickness[data='4']").addClass("unclickbtn");
            $(".clsInsideThickness[data='2']").addClass("unclickbtn");
            $(".clsInsideThickness[data='3']").addClass("unclickbtn");



            if ($("#FR4Type").val() == "Aluminum board") {
               $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn").removeClass("active");
               $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn").removeClass("active");
               $(".clsInsideThickness[data='5']").addClass("unclickbtn").removeClass("active");
               $(".clsInsideThickness[data='6']").addClass("unclickbtn").removeClass("active");


               var radCopperThickness = $("input[name='radCopperThickness']");
               var radInsideThickness = $("input[name='radInsideThickness']");

               if (radCopperThickness == "5 oz Cu" || radCopperThickness == "6 oz Cu") {
                   $(".clsCopperThickness").removeClass("active");
                   $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                   $("input[name='radCopperThickness']").val("1 oz Cu")
               }


               if (radCopperThickness == "5" || radCopperThickness == "6") {

                   $(".clsInsideThickness").removeClass("active");
                   $(".clsInsideThickness[data='1']").addClass("active");
                   $("input[name='radInsideThickness']").val("1")
               }
            } else {
               $(".clsCopperThickness[data='5 oz Cu']").removeClass("unclickbtn");
               $(".clsCopperThickness[data='6 oz Cu']").removeClass("unclickbtn");


               $(".clsInsideThickness[data='5']").removeClass("unclickbtn");
               $(".clsInsideThickness[data='6']").removeClass("unclickbtn");
            }

        }


        $("input[name='radCopperThickness']").val($(this).attr("data"));

        forLayerMaterialChange();
    })


    $(".clsTCE").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsTCE").removeClass("active");

        $(this).addClass("active");

        $("input[name='hidTCE']").val($(this).attr("data"));
    })

    $(".clsRogers").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsRogers").removeClass("active");

        $(this).addClass("active");

        $("input[name='hidRogers']").val($(this).attr("data"));
    })


    //Via Process
    $(".clsSolderCover").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsSolderCover").removeClass("active");

        $(this).addClass("active");

        $("input[name='radSolderCover']").val($(this).attr("data"));
    })

    //Stiffener
    $(".clsStiffener").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsStiffener").removeClass("active");

        $(this).addClass("active");


        if ($(this).attr("data") == "Without") {
            $("#dvPIMetalLengh").hide();
            $("#dvPIMetalLenghBOT").hide();
            $('#dvPIMetalLenghP').hide();
        }
        else if ($(this).attr("data") == "TOP") {
            $("#dvPIMetalLengh").show();
            $("#dvPIMetalLenghBOT").hide();
            $('#dvPIMetalLenghP').show();
        }
        else if ($(this).attr("data") == "BOT") {
            $("#dvPIMetalLenghBOT").show();
            $("#dvPIMetalLengh").hide();
            $('#dvPIMetalLenghP').show();
        }
        else if ($(this).attr("data") == "Both sides") {
            $("#dvPIMetalLengh").show();
            $("#dvPIMetalLenghBOT").show();
            $('#dvPIMetalLenghP').show();
        }

        $("input[name='radStiffener']").val($(this).attr("data"));
    })


    //Structure of MCPCB
    $(".clsStructure").click(function () {
        $(".clsStructure").addClass("active");
        $(this).addClass("active");
        $("#hidStructure").val($(this).attr("data"));
    })


    //
    $(".clsGoldfingers").click(goldFingerClick);

    function goldFingerClick() {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }


        $(".clsGoldfingers").removeClass("active");

        $(this).addClass("active");

        $("input[name='radGoldfingers']").val($(this).attr("data"));


        if (online.PageType == 4) {
            if ($(this).attr("data") == "No") {
                // $("#dvPIMetalLengh").hide();


                $(".clsStiffener[data='Without']").removeClass("unclickbtn");
            } else if ($(this).attr("data") == "Yes") {


                if ($("input[name='radStiffener']").val() == "Without") {
                    $("#dvPIMetalLengh").show();
                    $('#dvPIMetalLenghP').show();
                    $(".clsStiffener").removeClass("active").removeClass("unclickbtn");

                    $(".clsStiffener[data='Without']").addClass("unclickbtn");
                    $(".clsStiffener[data='TOP']").addClass("active");

                    $("input[name='radStiffener']").val("TOP");
                }
                else {
                    if ($(".clsStiffener[data='Without']").attr("class").indexOf("unclickbtn") == -1) {
                        $(".clsStiffener[data='Without']").addClass("unclickbtn");
                    }
                }



            }
        } else {
            if ($(this).attr("data") == "No") {
                //$("#dvPIMetalLengh").hide();


                $(".clsChamferedborder").hide();
                //$("#divGoldFingersThickness").hide();


                if ($("input[name='radBoardThickness']").val() == "0.4" || $("input[name='radBoardThickness']").val() == "0.2") {

                    $(".clsPlatingType").removeClass("unclickbtn");
                    $(".clsPlatingType").removeClass("active");

                    $(".clsPlatingType[data='HASL with lead']").addClass("unclickbtn");
                    $(".clsPlatingType[data='HASL lead free']").addClass("unclickbtn");

                    $(".clsPlatingType[data='Immersion gold']").addClass("active");
                    $("input[name='radPlatingType']").val("Immersion gold");
                   // $("#divGoldThickness").show();
                   // $(".clsGoldThickness").removeClass("active");
                   // $(".clsGoldThickness[data='1']").addClass("active");
                  //  $("input[name='radGoldThickness']").val("1");
                } else {
                    $(".clsPlatingType").removeClass("unclickbtn");

                    $(".clsPlatingType").removeClass("active");
                    $(".clsPlatingType[data='HASL with lead']").addClass("active");
                    $("input[name='radPlatingType']").val("HASL with lead");
                   // $(".clsGoldThickness").removeClass("active");
                  //  $(".clsGoldThickness[data='1']").addClass("active");
                  //  $("input[name='radGoldThickness']").val("1");
                   // $("#divGoldThickness").hide();
                }

               



            } else if ($(this).attr("data") == "Yes") {


                var oldradGoldPlatingType = $("input[name='radGoldPlatingType']").val();


               // $("#divGoldFingersThickness").hide();
                $(".clsChamferedborder").show();
                $(".clsGoldPlatingType").removeClass("active");
                $(".clsGoldPlatingType[data='Hard Gold']").addClass("active").removeClass("unclickbtn");
                $("input[name='radGoldPlatingType']").val("Hard Gold");
     
                //$(".clsPlatingType[data='Hard Gold']").addClass("unclickbtn");
                $(".clsPlatingType[data='None']").addClass("unclickbtn");
             

                //if (oldradGoldPlatingType!=)


                if ($("input[name='radBoardThickness']").val() == "0.4" || $("input[name='radBoardThickness']").val() == "0.2") {
                    $(".clsPlatingType").removeClass("active");
                    $(".clsPlatingType[data='Immersion gold']").addClass("active").removeClass("unclickbtn");
                    $("input[name='radPlatingType']").val("Immersion gold");
                   // $("#divGoldFingersThickness").show();

                   // $("#divGoldThickness").show();
                    //$(".clsGoldThickness").removeClass("active");
                    //$(".clsGoldThickness[data='1']").addClass("active");
                    //$("input[name='radGoldThickness']").val("1");

                   // var radGoldThickness = $("input[name='radGoldThickness']").val();
                   // if ($("input[name='radGoldPlatingType']").val() == "Immersion gold")
                  //  {
                    //    $(".clsGoldFingersThickness").removeClass("active");
                    //    $(".clsGoldFingersThickness[data='" + radGoldThickness + "']").addClass("active");
                   //     $("input[name='radGoldFingersThickness']").val(radGoldThickness);
                      
                  //  }

                }
                else {
                    $(".clsPlatingType").removeClass("active");
                    $(".clsPlatingType").removeClass("unclickbtn");

                    $(".clsPlatingType[data='None']").addClass("unclickbtn");
                    //$(".clsPlatingType[data='Hard Gold']").addClass("unclickbtn");
                   // $(".clsGoldThickness").removeClass("active");
                  //  $(".clsGoldThickness[data='1']").addClass("active");
                  //  $("input[name='radGoldThickness']").val("1");
                  //  $("#divGoldThickness").hide();



                    $(".clsPlatingType[data='HASL with lead']").addClass("active").removeClass("unclickbtn");
                    $("input[name='radPlatingType']").val("HASL with lead");
                }



            }


            if ($("input[name='radBoardThickness']").val() == "0.4" || $("input[name='radBoardThickness']").val() == "0.2" || $("input[name='radBoardThickness']").val() == "0.6") {
                $(".liChamferedborderYes").hide();
                $("input[name='hidChamferedborder']").val("No");
                $("#aChamferedborder").text("No");
            } else {
                $(".liChamferedborderYes").show();

            }



            if ($("input[name='radBoardThickness']").val() == "0.4" || $("input[name='radBoardThickness']").val() == "0.2") {


                $(".clsGoldPlatingType[data='HASL with lead']").addClass("unclickbtn");
                $(".clsGoldPlatingType[data='HASL lead free']").addClass("unclickbtn");


            } else {

                $(".clsGoldPlatingType[data='HASL with lead']").removeClass("unclickbtn");
                $(".clsGoldPlatingType[data='HASL lead free']").removeClass("unclickbtn");
            }


            if ($("input[name='radPlatingType']").val() == "HASL with lead" || $("input[name='radPlatingType']").val() == "HASL lead free") {
                if (getCookie("logFlag") == 1) {
                    $(".custom-weight-show").show();
                }
            }
            else {
                $(".custom-weight-show").hide();

            }


            if ($(this).attr("data") == "No") {

                if ($("input[name='FR4Type']").val() == "Rigid-Flex") {
                    $(".clsPlatingType").removeClass("unclickbtn");
                    $(".clsPlatingType").removeClass("active");

                    $(".clsPlatingType[data='HASL with lead']").addClass("unclickbtn");
                    $(".clsPlatingType[data='HASL lead free']").addClass("unclickbtn");
                    $(".clsPlatingType[data='Hard Gold']").addClass("unclickbtn");
                    $(".clsPlatingType[data='None']").addClass("unclickbtn");

                    $(".clsPlatingType[data='Immersion gold']").addClass("active");
                    $("input[name='radPlatingType']").val("Immersion gold");
                    $("#divGoldThickness").show();
                    $(".clsGoldThickness").removeClass("active");
                    $(".clsGoldThickness[data='1']").addClass("active");
                    $("input[name='radGoldThickness']").val("1");
                }
            } else {
                if ($("input[name='FR4Type']").val() == "Rigid-Flex") {
                    $(".clsPlatingType").removeClass("unclickbtn");
                    $(".clsPlatingType").removeClass("active");

                    $(".clsPlatingType[data='HASL with lead']").addClass("unclickbtn");
                    $(".clsPlatingType[data='HASL lead free']").addClass("unclickbtn");
                    $(".clsPlatingType[data='Hard Gold']").addClass("unclickbtn");
                    $(".clsPlatingType[data='None']").addClass("unclickbtn");

                    $(".clsPlatingType[data='Immersion gold']").addClass("active");
                    $("input[name='radPlatingType']").val("Immersion gold");




                    $(".clsGoldPlatingType").removeClass("unclickbtn");
                    $(".clsGoldPlatingType").removeClass("active");

                    $(".clsGoldPlatingType[data='HASL with lead']").addClass("unclickbtn");
                    $(".clsGoldPlatingType[data='HASL lead free']").addClass("unclickbtn");
                    $(".clsGoldPlatingType[data='Hard Gold']").addClass("unclickbtn");
                    $(".clsGoldPlatingType[data='None']").addClass("unclickbtn");

                    $(".clsGoldPlatingType[data='Immersion gold']").addClass("active");
                    $("input[name='radGoldPlatingType']").val("Immersion gold");
                    $("#divGoldThickness").show();
                    $(".clsGoldThickness").removeClass("active");
                    $(".clsGoldThickness[data='1']").addClass("active");
                    $("input[name='radGoldThickness']").val("1");

                }
            }


            forLayerMaterialChange();
            SurfaceFinishChange();
        }


    }



    ////Stiffener
    //$(".clsCuLayers").click(function () {

    //    if ($(this).attr("class").indexOf("unclickbtn") > -1) {
    //        return;
    //    }

    //    $(".clsCuLayers").removeClass("active");

    //    $(this).addClass("active");

    //    $("input[name='radCuLayers']").val($(this).attr("data"));
    //})

    //MarkingPrint
    $('.clsMarkingPrint').click(function () {
        if ($(this).attr('class').indexOf("unclickbtn") > -1) {
            return;
        }
        $('.clsMarkingPrint').removeClass('active');

        $(this).addClass('active');

        $('input[name="radMarkingPrint"]').val($(this).attr('data'));
    })

    $(".ui2-dropdown-customize").hover(function () {

        $(this).children().eq(2).show();
    }, function () {
        $(this).children().eq(2).hide()
    })

    $(".ui2-popup-menu li").click(function () {
        //debugger;
        if ($(this).parent().parent().find(".ui2-dropdown-layout").find("a").length == 0) {
            if ($(this).parent().parent().find('input[name="radPILength"]').length > 0 || $(this).parent().parent().find('input[name="radFrLength"]').length > 0 || $(this).parent().parent().find('input[name="radStiffMetalLength"]').length > 0) {
                var vval = $(this).attr("data");
                if (vval == 0) {
                    vval = '--';
                }
                else {
                    vval = vval + 'mm';
                }
                $(this).parent().parent().find("input").eq(0).val(vval);
                $(this).parent().parent().find("input").eq(1).val($(this).attr("data"));
            } else if ($(this).parent().parent().find('input[name="hidTesatape"]').length > 0) {
                $(this).parent().parent().find("input").eq(0).val($(this).find('a').text());
                $(this).parent().parent().find("input").eq(1).val($(this).attr("data"));
                if ($(this).attr("data") == 'without') {
                    $('#divEdgeRails').hide();
                }
                else {
                    $('#divEdgeRails').show();
                }
            } else if ($(this).parent().parent().find('input[name="radEMIShieldingFilm"]').length > 0) {
                $(this).parent().parent().find("input").eq(0).val($(this).find('a').text());
                $(this).parent().parent().find("input").eq(1).val($(this).attr("data"));
                if ($(this).attr("data") == 'without') {
                    $('#divEMIShieldingFilm').hide();
                }
                else {
                    $('#divEMIShieldingFilm').show();
                }
            } else {
                $(this).parent().parent().find("input").eq(0).val($(this).text());
                $(this).parent().parent().find("input").eq(1).val($(this).attr("data"));
            }



            $("#iptCusomFirst").focus().click();


            if ($("input[name='FR4Type']").val() == "Aluminum board") {
                if ($(this).attr("class") && $(this).attr("class").indexOf("clsTopLayer") > -1) {

                    $(".clsBottomSize").hide();
                    $(".clsTopSize").show();


                } else if ($(this).attr("class") && $(this).attr("class").indexOf("clsBottomLayer") > -1) {
                    $(".clsBottomSize").show();

                    $(".clsTopSize").hide();

                } else if ($(this).attr("class") && $(this).attr("class").indexOf("clsNoneLayer") > -1) {
                    $(".clsBottomSize").show();
                    $(".clsTopSize").show();

                }
            }

        }
        else {


            //Size change
            if ($(this).attr("class") && $(this).attr("class").indexOf("clsLiSize") > -1) {


                $(this).parent().parent().find("a").eq(0).text($(this).text());
                $(this).parent().parent().find("input").eq(0).val($(this).attr("data"));
                // 钢网产品的ID计价用
                $(this).parent().parent().find("input").eq(1).val($(this).attr("proid"));

            } else if ($(this).attr("class") && $(this).attr("class").indexOf("clscustomize") > -1) {
                //if ($(this).text() == "No") {
                //    $("#iptEdgeRailsLen").attr("disabled", true);
                //}
                //else {
                //    $("#iptEdgeRailsLen").attr("disabled", false).focus();


                //}


                //$(this).parent().parent().find("a").eq(0).text($(this).text());
                //$(this).parent().parent().find("input").eq(0).val($(this).text());

                //$("#iptEdgeRailsLen").val($(this).attr("data"));
                //$("input[name='iptEdgeRailsLen']").val($(this).attr("data"));
                $(this).parent().parent().find("a").eq(0).text($(this).text());
                $(this).parent().parent().find("input").eq(0).val($(this).attr("data"));


            }
            else {

                $(this).parent().parent().find("a").eq(0).text($(this).text());
                $(this).parent().parent().find("input").eq(0).val($(this).text());


            }



        }



        $(this).parent().hide();
    })








    //SMD-Stencil 
    //Stencil type
    $(".clsStencilType").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsStencilType").removeClass("active");

        $(this).addClass("active");

        $("input[name='radStencilType']").val($(this).attr("data"));

        if ($(this).attr("data") == "Framework") {
            $(".clsLiSize").show();
            $(".clsLiSizeNone").hide();

            $("#hidSize").val($(".clsLiSize").find("a").eq(0).text());

            $("#divStencilSize").find("a").html($(".clsLiSize").find("a").eq(0).html());
            $("#radStencilId").val($(".clsLiSize").eq(0).attr("data"));
            $("#hidStencilId").val("7");
        }
        else {
            $(".clsLiSize").hide();
            $(".clsLiSizeNone").show();


            $("#hidSize").val($(".clsLiSizeNone").find("a").eq(0).text());

            $("#divStencilSize").find("a").html($(".clsLiSizeNone").find("a").eq(0).html());
            $("#radStencilId").val($(".clsLiSizeNone").eq(0).attr("data"));
            $("#hidStencilId").val("9");

        }
    })

    $(".clsStencilSide").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsStencilSide").removeClass("active");

        $(this).addClass("active");

        $("input[name='radStencilSide']").val($(this).attr("data"));


        if ($(this).attr("data") == "Separate Top Bottom") {
            $("#iptStencilNum").val(2);
            $("input[name=iptStencilNum]").val(2);
        }
        else {
            $("#iptStencilNum").val(1);
            $("input[name=iptStencilNum]").val(1);

        }

    })

    $("#iptStencilNum").blur(function () {
        if ($("input[name='radStencilSide']").val() == "Separate Top Bottom") {
            $(this).val(2);
            $("input[name=iptStencilNum]").val(2);
        }
    })

    $(".clsStencilThickness").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsStencilThickness").removeClass("active");

        $(this).addClass("active");

        $("input[name='radStencilThickness']").val($(this).attr("data"));




    })

    $(".clsExistingfiducials").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsExistingfiducials").removeClass("active");

        $(this).addClass("active");

        $("input[name='radExistingfiducials']").val($(this).attr("data"));


    })


    $(".clsElectropolishing").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsElectropolishing").removeClass("active");

        $(this).addClass("active");

        $("input[name='radElectropolishing']").val($(this).attr("data"));


    })

    $(".clsETest").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsETest").removeClass("active");

        $(this).addClass("active");

        $("input[name='radETest']").val($(this).attr("data"));

    });


    $('.flexopt').click(function () {
        $('.flexopt.tb-selected').removeClass('tb-selected');
        $(this).addClass('tb-selected');

        $('input[name="partVias"]').removeAttr('checked');
        $(this).prev().prop('checked', true);
    });

    $(".clsPCBRelation").click(function () {

        if ($(this).attr("class").indexOf("unclickbtn") > -1) {
            return;
        }

        $(".clsPCBRelation").removeClass("active");

        $(this).addClass("active");
        $('#PCBRelationTip').hide();
    })

});





//function BoardTypeClick(val)
//{
//    if (val == "Single PCB") {
//        $("#trAcceptCrossed").hide();
//    }
//    else {
//        $("#trAcceptCrossed").show();
//    }

//}


function SetFR4(id, val, obj) {


    if (val == "Aluminum board" || val == "Copper") {
        $(".lblhidULMaker").hide();
    } else {
        $(".lblhidULMaker").show();

    }
   
    $(".clsBottomSize").show();
    $(".clsTopSize").show();
    var layer = $("input[name=hidLayers]").val();
    var isvl = val.toString().indexOf("Aluminum") > -1;
    if (layer != 1  && val.toString().indexOf("Aluminum") > -1) {

        $(".clshidLayers[data='1']").click();
        $(".clsMaterial[data='2']").click();
        return;
        //alert("For AL Board, only can choose single layer"); return;

    }
    else if (layer != 1 && layer != 2 && val.toString().indexOf("Copper") > -1) {

        $(".clshidLayers[data='1']").click();
        $(".clsMaterial[data='5']").click();
        return;
        //alert("For AL Board, only can choose single layer"); return;

    }

    if (obj && $(obj).attr("class").indexOf("unclickbtn") > -1) {
        return;
    }



    if (val == "FR-4") {
        $(".clsCopperThickness[data='4 oz Cu']").removeClass("unclickbtn");
        $(".clsCopperThickness[data='2 oz Cu']").removeClass("unclickbtn");

        $(".clsInsideThickness[data='4']").removeClass("unclickbtn");
        $(".clsInsideThickness[data='2']").removeClass("unclickbtn");
    }


    if ((layer == 1 || layer == 2) && isvl) {

        $("#trtg").hide(); $("#hidFR4TG").val("TG130"); $(".clsBothSide").hide();

        if (layer == 1) {
            if ($("input[name='hidSoldermask']").val() == "Both sides") {
                $("input[name='hidSoldermask']").val('--');
                $("input[name='hidSoldermask']").prev().val('--');
            }

            if ($("input[name='hidSilkscreenLegend']").val() == "Both sides") {
                $("input[name='hidSilkscreenLegend']").val('--');
                $("input[name='hidSilkscreenLegend']").prev().val('--');
            }


            if ($("input[name='hidCopperlayer']").val() == "Top layer") {
                $(".clsBottomSize").hide();
                $(".clsTopSize").show();

                if ($("input[name='hidSoldermask']").val() == "Bottom side") {
                    $("input[name='hidSoldermask']").val('--');
                    $("input[name='hidSoldermask']").prev().val('--');
                }

                if ($("input[name='hidSilkscreenLegend']").val() == "Bottom side") {
                    $("input[name='hidSilkscreenLegend']").val('--');
                    $("input[name='hidSilkscreenLegend']").prev().val('--');
                }

            } else if ($("input[name='hidCopperlayer']").val() == "Bottom layer") {
                $(".clsBottomSize").show();
                $(".clsTopSize").hide();

                if ($("input[name='hidSoldermask']").val() == "Top side") {
                    $("input[name='hidSoldermask']").val('--');
                    $("input[name='hidSoldermask']").prev().val('--');
                }

                if ($("input[name='hidSilkscreenLegend']").val() == "Top side") {
                    $("input[name='hidSilkscreenLegend']").val('--');
                    $("input[name='hidSilkscreenLegend']").prev().val('--');
                }

            }
        } else if (layer == 2) {
            $("#divStructure").show();
        }
    } else {
        $("#trtg").show(); $(".clsBothSide").show();
        $("#divStructure").hide();
    }

    var cl = $("#FR4Type").val();
    if (id == "hidFR4TG" && cl.indexOf("Aluminum") > -1 && val != "TG130") {
        alert("For AL Board, only can choose TG130"); return;
    }


    $("#" + id).val(val);
    $(obj).parent().find("a").removeClass("active");
    $(obj).addClass("active");

    //$(obj).parent().parent().find("li").removeClass("tb-selected");

    //$(obj).parent().addClass("tb-selected");

    if (val.toString().indexOf("Aluminum") > -1) {

        //$(".clsCopperThickness").removeClass("active").removeClass("unclickbtn");

        //$("input[name='radSolderColor']").val("White");
        //$(".clsCopperThickness[data='White']").addClass("active");

        var valradBoardThickness = $("input[name='radBoardThickness']").val();
        //if (valradBoardThickness == "0.4" || valradBoardThickness == "0.6" || valradBoardThickness == "0.8" || valradBoardThickness == "2.4") {
        //    $(".clsBoardThickness[data='1.6']").addClass("active").removeClass("unclickbtn");
        //    $("input[name='radBoardThickness']").val(1.6)

        //}
        //$(".clsBoardThickness[data='0.4']").removeClass("active").addClass("unclickbtn");
        //$(".clsBoardThickness[data='0.6']").removeClass("active").addClass("unclickbtn");
        //$(".clsBoardThickness[data='0.8']").removeClass("active").addClass("unclickbtn");
        //$(".clsBoardThickness[data='2.4']").removeClass("active").addClass("unclickbtn");
     
        //$(".clsBoardThickness[data='0.2']").removeClass("active").addClass("unclickbtn");

        //if (valradBoardThickness == 0.2)
        //{
        //    $(".clsBoardThickness[data='1.6']").addClass("active").removeClass("unclickbtn");
        //    $("input[name='radBoardThickness']").val(1.6);
        //}


        $(".clsFontColor").removeClass("active").removeClass("unclickbtn");
        $("input[name='radFontColor']").val("Black");
        $(".clsFontColor[data='Black']").addClass("active");
        $(".clsFontColor[data='White']").addClass("unclickbtn");



        $(".clsSolderColor").removeClass("active").removeClass("unclickbtn");
        $("input[name='radSolderColor']").val("White");
        $(".clsSolderColor[data='White']").addClass("active");


        $(".clsCopperThickness").removeClass("active").removeClass("unclickbtn");

        $("input[name='radCopperThickness']").val("1 oz Cu");
        $(".clsCopperThickness[data='1 oz Cu']").addClass("active");

        //$(".clsCopperThickness[data='1 oz Cu']").addClass("unclickbtn");
        //$(".clsCopperThickness[data='2 oz Cu']").addClass("unclickbtn");
        // $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");
        $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");

        //$("input[name='radBoardThickness'][value='0.4']").attr("disabled", true);
        //$("input[name='radBoardThickness'][value='0.6']").attr("disabled", true);
        //$("input[name='radBoardThickness'][value='0.8']").attr("disabled", true);
        //$("input[name='radBoardThickness'][value='2.4']").attr("disabled", true);

        //$("input[name='radFontColor'][value='Black']").attr("disabled", false);
        //$("input[name='radFontColor'][value='Black']").attr("checked", true);
        //$("input[name='radSolderColor'][value='White']").attr("disabled", false);
        //$("input[name='radSolderColor'][value='White']").attr("checked", true);
        //$("input[name='radCopperThickness'][value='1 oz Cu']").attr("checked", true);
        //$("input[name='radCopperThickness'][value='2 oz Cu']").attr("disabled", true);
        //$("input[name='radCopperThickness'][value='3 oz Cu']").attr("disabled", true);
        //$("input[name='radCopperThickness'][value='4 oz Cu']").attr("disabled", true);
    }

    if (layer == "1" && $("#FR4Type").val() == "Aluminum board") {
        $(".clsVias").removeClass("unclickbtn");
        $(".clsVias[data='0.15']").removeClass("unclickbutton");
        $(".clsVias[data='0.2']").removeClass("unclickbutton");

        $(".clsVias[data='0.25']").removeClass("unclickbutton");
        $(".clsVias[data='0.3']").removeClass("unclickbutton");
        $(".clsVias[data='0.8']").addClass("unclickbtn");
        $(".clsVias[data='1.0']").addClass("unclickbtn");
        $(".clsVias[data='-1']").addClass("unclickbtn");
        $(".clsLineWeight[data='3/3mil']").addClass("unclickbtn").removeClass("active");
        if ($("input[name='radVias']").val() != "0.8" && $("input[name='radVias']").val() != "1.0") {
            $("input[name='radVias']").val('0.8');
            $(".clsVias").removeClass("active");
            $(".clsVias[data='0.8']").removeClass("active");
        }
    }
    else if (layer == "2" && $("#FR4Type").val() == "Aluminum board") {
        // $(".clsVias").removeClass("unclickbtn");
        $(".clsVias[data='0.15']").removeClass("unclickbutton");
        $(".clsVias[data='0.2']").removeClass("unclickbutton");
        $(".clsVias[data='0.25']").removeClass("unclickbutton");
        $(".clsVias[data='0.3']").removeClass("unclickbutton");
        $(".clsVias[data='0.8']").addClass("unclickbtn");
        $(".clsVias[data='1.0']").addClass("unclickbtn");
        $(".clsVias[data='-1']").addClass("unclickbtn");
        $(".clsLineWeight[data='3/3mil']").addClass("unclickbtn").removeClass("active");
        if ($("input[name='radVias']").val() != "1.0") {
            $("input[name='radVias']").val('1.0');
            $(".clsVias").removeClass("active");
            $(".clsVias[data='1.0']").removeClass("active");
        }
    }
    else {
        $(".clsVias").removeClass("unclickbtn");
        $(".clsVias[data='0.8']").addClass("unclickbtn");
        $(".clsVias[data='1.0']").addClass("unclickbtn");
        if ($("input[name='radVias']").val() != "0.2" && $("input[name='radVias']").val() != "0.25" && $("input[name='radVias']").val() != "0.3") {
            $("input[name='radVias']").val('0.3');
            $(".clsVias").removeClass("active");
            $(".clsVias[data='0.3']").addClass("active");
        }
    }




    if (val == "FR-4") {


        var radGoldfingers = $("input[name='radGoldfingers']").val();
        if (radGoldfingers == "No") {
            $(".clsGoldfingers[data='No']").click();
        } else {
            $(".clsGoldfingers[data='Yes']").click();

        }
    }
    else if (val == "Rigid-Flex") {

        var radGoldfingers = $("input[name='radGoldfingers']").val();
        if (radGoldfingers == "No") {
            $(".clsGoldfingers[data='No']").click();
        } else {
            $(".clsGoldfingers[data='Yes']").click();

        }
    }

    forLayerMaterialChange();
    SurfaceFinishChange();

}

function SurfaceFinishChange(type)
{

    var chkPCB = $("input[name='chkPCB']").is(':checked') ? "on" : "";

    if (online.PageType == 1 || chkPCB == "on") {

        var radGoldfingers = $("input[name='radGoldfingers']").val();
        var radGoldPlatingType = $("input[name='radGoldPlatingType']").val();
        var radPlatingType = $("input[name='radPlatingType']").val();
        var radGoldFingersThickness = $("input[name='radGoldFingersThickness']").val();
        var radGoldThickness = $("input[name='radGoldThickness']").val();
        var radBoardThickness = parseFloat($("input[name='radBoardThickness']").val());


        var radAUGoldThickness = $("input[name='radAUGoldThickness']").val();
        var radNiGoldThickness = $("input[name='radNiGoldThickness']").val();

        var radPdAUGoldThickness = $("input[name='radPdAUGoldThickness']").val();
        var radPdNiGoldThickness = $("input[name='radPdNiGoldThickness']").val();
        var radPdPdGoldThickness = $("input[name='radPdPdGoldThickness']").val();


        if (radGoldfingers == "Yes") {
            if (radGoldPlatingType == "Immersion gold") {
                $("#divGoldFingersThickness").show();

                if (radPlatingType == "Immersion gold") {
                    $(".clsGoldThickness").removeClass("active").addClass("unclickbtn");
                    $(".clsGoldThickness[data='" + radGoldFingersThickness + "']").removeClass("unclickbtn").addClass("active");
                    $("input[name='radGoldThickness']").val(radGoldFingersThickness);

                    


                }
            }
            else {
                $("#divGoldFingersThickness").hide();

                if (radPlatingType == "Immersion gold") {
                    $(".clsGoldThickness").removeClass("unclickbtn");

                }
            }


            if (radGoldPlatingType == "Hard Gold") {
                $("#divNiGoldThickness").show();

            } else {
                $("#divNiGoldThickness").hide();
               
            }
            
           

            if (radGoldPlatingType == "ENEPIG") {
                $("#divPdGoldThickness").show();

            } else {
                $("#divPdGoldThickness").hide();

            }


            if (radBoardThickness > 0.4) {
                if (radGoldPlatingType == "Immersion gold" || radGoldPlatingType == "Hard Gold" || radGoldPlatingType == "HASL lead free") {
                    $(".clsPlatingType[data='HASL lead free']").removeClass("unclickbtn");
                }
                else {
                    $(".clsPlatingType[data='HASL lead free']").addClass("unclickbtn");

                    if (radPlatingType == "HASL lead free")
                    {
                        $(".clsPlatingType").removeClass("active");

                        $(".clsPlatingType[data='Immersion gold']").addClass("active");
                        $("input[name='radPlatingType']").val("Immersion gold");

                    }
                }

               
            } else {
                $(".clsPlatingType[data='HASL lead free']").addClass("unclickbtn");

                $(".clsPlatingType[data='HASL with lead']").addClass("unclickbtn");

                if (radPlatingType == "HASL lead free" || radPlatingType == "HASL with lead") {
                    $(".clsPlatingType").removeClass("active");

                    $(".clsPlatingType[data='Immersion gold']").addClass("active");
                    $("input[name='radPlatingType']").val("Immersion gold");

                }
            }

        }
        else {
            $("#divGoldFingersThickness").hide();
            $("#divNiGoldThickness").hide();

            if (radPlatingType == "Immersion gold") {
                $(".clsGoldThickness").removeClass("unclickbtn");
               

            }

        }


        if (radPlatingType == "Hard Gold") {

            $("#divNiSecondGoldThickness").show();
        } else {
            $("#divNiSecondGoldThickness").hide();
        }


        if (radPlatingType == "ENEPIG") {

            $("#divPdSecondGoldThickness").show();
        } else {
            $("#divPdSecondGoldThickness").hide();
        }



        if (radPlatingType != "Immersion gold") {
            $("#divGoldThickness").hide();
        } else {
            $("#divGoldThickness").show();
        }

     
        
        if (radGoldfingers == "Yes" && radGoldPlatingType == "Hard Gold") {
            $(".dvBtnSelAuNiSencond").hide();

            //if (type == 2)
            //{

                $("#aNiSecondGoldThickness").html('Au:' + radAUGoldThickness + 'U"/Ni:' + radNiGoldThickness + 'U"<i></i>');
                $("input[name='radSendAUGoldThickness']").val(radAUGoldThickness);
                $("input[name='radSendNiGoldThickness']").val(radNiGoldThickness);
              

            //}
        }
        else {
            if (radPlatingType == "Hard Gold")
            {
                $(".dvBtnSelAuNiSencond").show();

            }
        }
      

        if (radGoldfingers == "Yes" && radGoldPlatingType == "ENEPIG") {
            $(".dvBtnSelAuNiPdSencond").hide();

            //if (type == 2) {


                $("#aPdSecondGoldThickness").html('Ni:' + radPdNiGoldThickness + 'U"&nbsp;Pd:' + radPdPdGoldThickness + 'U"&nbsp;Au:' + radPdAUGoldThickness + 'U"<i></i>');
                $("input[name='radSendPdAUGoldThickness']").val(radPdAUGoldThickness);
                $("input[name='radSendPdNiGoldThickness']").val(radPdNiGoldThickness);
                $("input[name='radSendPdPdGoldThickness']").val(radPdPdGoldThickness);


            //}
        }
        else {
            if (radPlatingType == "ENEPIG") {
                $(".dvBtnSelAuNiPdSencond").show();

            }
        }



        if ($("#FR4Type").val() == "HDI")
        {
            radGoldfingers = $("input[name='radGoldfingers']").val();

            radGoldPlatingType = $("input[name='radGoldPlatingType']").val();
            radPlatingType = $("input[name='radPlatingType']").val();

            //if (radBoardThickness < 0.6) {
                if (radGoldfingers == "Yes") {
                    $(".clsGoldPlatingType[data='HASL with lead']").addClass("unclickbtn").removeClass('active');
                    $(".clsGoldPlatingType[data='HASL lead free']").addClass("unclickbtn").removeClass('active');

                    if (radGoldPlatingType == "HASL with lead" || radGoldPlatingType == "HASL lead free") {
                        $(".clsGoldPlatingType[data='Immersion gold']").removeClass("unclickbtn").addClass('active');
                        $("input[name='radGoldPlatingType']").val("Immersion gold");
                    }
                }


                $(".clsPlatingType[data='HASL with lead']").addClass("unclickbtn").removeClass('active');
                $(".clsPlatingType[data='HASL lead free']").addClass("unclickbtn").removeClass('active');

                if (radPlatingType == "HASL with lead" || radPlatingType == "HASL lead free") {
                    $(".clsPlatingType[data='Immersion gold']").removeClass("unclickbtn").addClass('active');
                    $("input[name='radPlatingType']").val("Immersion gold");
                }
            //}
            //else {
            //    if (radGoldfingers == "Yes") {
            //        $(".clsGoldPlatingType[data='HASL with lead']").addClass("unclickbtn").removeClass('active');


            //        if (radGoldPlatingType == "HASL with lead") {
            //            $(".clsGoldPlatingType[data='HASL lead free']").removeClass("unclickbtn").addClass('active');
            //            $("input[name='radGoldPlatingType']").val("HASL lead free");
            //        }
            //    }


            //    $(".clsPlatingType[data='HASL with lead']").addClass("unclickbtn").removeClass('active');

            //    if (radPlatingType == "HASL with lead") {
            //        $(".clsPlatingType[data='HASL lead free']").removeClass("unclickbtn").addClass('active');
            //        $("input[name='radPlatingType']").val("HASL lead free");
            //    }
            //}
        }
    }

}


function forLayerMaterialChange() {
    var chkPCB = $("input[name='chkPCB']").is(':checked') ? "on" : "";
  
    if (online.PageType == 1 || chkPCB == "on") {



        var hidLayers = $("input[name='hidLayers']").val();
        var FR4Type = $("input[name='FR4Type']").val();
        var radBoardThickness = $("input[name='radBoardThickness']").val();
        var radLineWeight = $("input[name='radLineWeight']").val();

        $(".clsBoardThickness").removeClass("unclickbtn");
        if (FR4Type == "FR-4" || FR4Type == "HDI") {
            $("#trtg").show();
            $("#divStructure").hide();
            $("#divTCE").hide();
            $("#divRogers").hide();
        }
        else if (FR4Type == "Aluminum board") {

            $(".clsTCE").removeClass("unclickbtn");
   

            if (hidLayers == 2) {
                $("#trtg").hide();
                $("#divStructure").show();
                $("#divTCE").show();
                $("#divRogers").hide();
                $("#divRogers").hide();
         
               
            } else {
                $("#trtg").hide();
                $("#divStructure").hide();
                $("#divTCE").show();
                $("#divRogers").hide();
                
            }

            $(".clsBoardThickness[data='0.2']").removeClass("active").addClass("unclickbtn");
            if (radBoardThickness == 0.2)
            {
                $(".clsBoardThickness[data='1.6']").addClass("active").removeClass("unclickbtn");
                $("input[name='radBoardThickness']").val(1.6);
            }
        }
        else if (FR4Type == "Copper") {
      
            $(".clsBoardThickness[data='0.2']").removeClass("active").addClass("unclickbtn");

            if (hidLayers == 2) {
                $("#trtg").hide();
                $("#divStructure").hide();
                $("#divTCE").show();
                $("#divRogers").hide();


                $(".clsBoardThickness[data='0.2']").removeClass("active").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.4']").removeClass("active").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.6']").removeClass("active").addClass("unclickbtn");
                $(".clsBoardThickness[data='2.4']").removeClass("active").addClass("unclickbtn");
                $(".clsBoardThickness[data='2.8']").removeClass("active").addClass("unclickbtn");
                $(".clsBoardThickness[data='3.2']").removeClass("active").addClass("unclickbtn");

                if (radBoardThickness < 0.8 || radBoardThickness>2.0) {
                    $(".clsBoardThickness[data='1.6']").addClass("active").removeClass("unclickbtn");
                    $("input[name='radBoardThickness']").val(1.6);
                }


            } else {
                $("#trtg").hide();
                $("#divStructure").hide();
                $("#divTCE").show();
                $("#divRogers").hide();

                $(".clsBoardThickness[data='0.2']").removeClass("active").addClass("unclickbtn");
                $(".clsBoardThickness[data='0.4']").removeClass("active").addClass("unclickbtn");
             
                $(".clsBoardThickness[data='2.4']").removeClass("active").addClass("unclickbtn");
                $(".clsBoardThickness[data='2.8']").removeClass("active").addClass("unclickbtn");
                $(".clsBoardThickness[data='3.2']").removeClass("active").addClass("unclickbtn");

                if (radBoardThickness < 0.6 || radBoardThickness > 2.0) {
                    $(".clsBoardThickness[data='1.6']").addClass("active").removeClass("unclickbtn");
                    $("input[name='radBoardThickness']").val(1.6);
                }
            }

           
            $(".clsTCE").removeClass("active").addClass("unclickbtn");
            $(".clsTCE[data='2.0']").addClass("active").removeClass("unclickbtn");
            $("input[name='hidTCE']").val("2.0");
        }
        else if (FR4Type == "Rogers") {

            $("#trtg").hide();
            $("#divStructure").hide();
            $("#divTCE").hide();
            $("#divRogers").show();

        } 
        if (hidLayers == 4) {

            // $(".clsInsideThickness[data='3']").addClass("unclickbtn");
            // $(".clsInsideThickness[data='4']").addClass("unclickbtn");
            // $(".clsInsideThickness[data='5']").addClass("unclickbtn");
            // $(".clsInsideThickness[data='6']").addClass("unclickbtn");

            $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
            if (radBoardThickness == 0.2) {
                $(".clsBoardThickness").removeClass("active");
                $(".clsBoardThickness[data='1.6']").addClass("active");

               

                $("input[name='radBoardThickness']").val("1.6");
            }
        
    }
        else if (hidLayers == 6) {
            $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.6']").addClass("unclickbtn");

            if (radBoardThickness == 0.2 || radBoardThickness == 0.4 || radBoardThickness == 0.6) {
                $(".clsBoardThickness").removeClass("active");
                $(".clsBoardThickness[data='1.6']").addClass("active");
                $("input[name='radBoardThickness']").val("1.6");
            }
        }
        else if (hidLayers == 8) {
            $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.6']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.8']").addClass("unclickbtn");

            if (radBoardThickness == 0.2 || radBoardThickness == 0.4 || radBoardThickness == 0.6 || radBoardThickness == 0.8) {
                $(".clsBoardThickness").removeClass("active");
                $(".clsBoardThickness[data='1.6']").addClass("active");
                $("input[name='radBoardThickness']").val("1.6");
            }

        }
        else if (hidLayers == 10) {
            $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.6']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.8']").addClass("unclickbtn");
            $(".clsBoardThickness[data='1.0']").addClass("unclickbtn");

            if (radBoardThickness == 0.2 || radBoardThickness == 0.4 || radBoardThickness == 0.6 || radBoardThickness == 0.8 || radBoardThickness == 1.0) {
                $(".clsBoardThickness").removeClass("active");
                $(".clsBoardThickness[data='1.6']").addClass("active");
                $("input[name='radBoardThickness']").val("1.6");
            }
        }
        else if (hidLayers == 12) {
            $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.6']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.8']").addClass("unclickbtn");
            $(".clsBoardThickness[data='1.0']").addClass("unclickbtn");
            $(".clsBoardThickness[data='1.2']").addClass("unclickbtn");

            if (radBoardThickness == 0.2 || radBoardThickness == 0.4 || radBoardThickness == 0.6 || radBoardThickness == 0.8 || radBoardThickness == 1.0 || radBoardThickness == 1.2) {
                $(".clsBoardThickness").removeClass("active");
                $(".clsBoardThickness[data='1.6']").addClass("active");
                $("input[name='radBoardThickness']").val("1.6");
            }

        }
        
        else if (hidLayers == 14) {
            $(".clsBoardThickness[data='0.2']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.4']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.6']").addClass("unclickbtn");
            $(".clsBoardThickness[data='0.8']").addClass("unclickbtn");
            $(".clsBoardThickness[data='1.0']").addClass("unclickbtn");
            $(".clsBoardThickness[data='1.2']").addClass("unclickbtn");
            $(".clsBoardThickness[data='1.6']").addClass("unclickbtn");


            if (radBoardThickness == 0.2 || radBoardThickness == 0.4 || radBoardThickness == 0.6 || radBoardThickness == 0.8 || radBoardThickness == 1.0 || radBoardThickness == 1.2 || radBoardThickness == 1.6) {
                $(".clsBoardThickness").removeClass("active");
                $(".clsBoardThickness[data='2.0']").addClass("active");
                $("input[name='radBoardThickness']").val("2.0");
            }
        }

         else {

        }

        var radCopperThickness = $("input[name='radCopperThickness']").val();
        var radInsideThickness = $("input[name='radInsideThickness']").val();
        radBoardThickness = $("input[name='radBoardThickness']").val();
        $(".clsCopperThickness").removeClass("unclickbtn");
        $(".clsInsideThickness").removeClass("unclickbtn");

        if (FR4Type == "Copper") {
     
            $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn");
            $(".clsCopperThickness[data='7 oz Cu']").addClass("unclickbtn");
            $(".clsCopperThickness[data='8 oz Cu']").addClass("unclickbtn");
            $(".clsCopperThickness[data='9 oz Cu']").addClass("unclickbtn");
            $(".clsCopperThickness[data='10 oz Cu']").addClass("unclickbtn");
            $(".clsCopperThickness[data='11 oz Cu']").addClass("unclickbtn");
            $(".clsCopperThickness[data='12 oz Cu']").addClass("unclickbtn");
            $(".clsCopperThickness[data='13 oz Cu']").addClass("unclickbtn");


            if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 6) {
                $(".clsCopperThickness").removeClass("active");
                $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                $("input[name='radCopperThickness']").val("1 oz Cu")

            }

        }
        else if (FR4Type == "Aluminum board") {
            if (radBoardThickness == 0.2) {
                $(".clsCopperThickness[data='2 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='7 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='8 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='9 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='10 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='11 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='12 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='13 oz Cu']").addClass("unclickbtn");
        


                $(".clsInsideThickness[data='2']").addClass("unclickbtn");
                $(".clsInsideThickness[data='3']").addClass("unclickbtn");
                $(".clsInsideThickness[data='4']").addClass("unclickbtn");
                $(".clsInsideThickness[data='5']").addClass("unclickbtn");
                $(".clsInsideThickness[data='6']").addClass("unclickbtn");



                if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 2) {
                    $(".clsCopperThickness").removeClass("active");
                    $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                    $("input[name='radCopperThickness']").val("1 oz Cu")

                }


                if (radInsideThickness == "2" || radInsideThickness == "3" || radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {


                    $(".clsInsideThickness").removeClass("active");
                    $(".clsInsideThickness[data='1']").addClass("active");
                    $("input[name='radInsideThickness']").val("1")
                }

            }
            else if (radBoardThickness == 0.4) {

                if (radLineWeight != "8/8mil") {
                    $(".clsCopperThickness[data='2 oz Cu']").addClass("unclickbtn");
                    $(".clsInsideThickness[data='2']").addClass("unclickbtn");
                }

                $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn");
             
                $(".clsCopperThickness[data='7 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='8 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='9 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='10 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='11 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='12 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='13 oz Cu']").addClass("unclickbtn");





                $(".clsInsideThickness[data='3']").addClass("unclickbtn");
                $(".clsInsideThickness[data='4']").addClass("unclickbtn");
                $(".clsInsideThickness[data='5']").addClass("unclickbtn");
                $(".clsInsideThickness[data='6']").addClass("unclickbtn");

                if (radLineWeight != "8/8mil") {
                    if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 2) {
                        $(".clsCopperThickness").removeClass("active");
                        $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                        $("input[name='radCopperThickness']").val("1 oz Cu")

                    }


                    if (radInsideThickness == "2" || radInsideThickness == "3" || radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {


                        $(".clsInsideThickness").removeClass("active");
                        $(".clsInsideThickness[data='1']").addClass("active");
                        $("input[name='radInsideThickness']").val("1")
                    }
                }
                else {
                    if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 3) {
                        $(".clsCopperThickness").removeClass("active");
                        $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                        $("input[name='radCopperThickness']").val("1 oz Cu")

                    }


                    if (radInsideThickness == "3" || radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {


                        $(".clsInsideThickness").removeClass("active");
                        $(".clsInsideThickness[data='1']").addClass("active");
                        $("input[name='radInsideThickness']").val("1")
                    }
                }



            }
            else if (radBoardThickness == 0.6 || radBoardThickness == 0.8) {

                if (radLineWeight != "8/8mil") {
                    $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");
                    $(".clsInsideThickness[data='3']").addClass("unclickbtn");
                }


                $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='7 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='8 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='9 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='10 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='11 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='12 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='13 oz Cu']").addClass("unclickbtn");




                $(".clsInsideThickness[data='4']").addClass("unclickbtn");
                $(".clsInsideThickness[data='5']").addClass("unclickbtn");
                $(".clsInsideThickness[data='6']").addClass("unclickbtn");

                if (radLineWeight != "8/8mil") {
                    if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 3) {
                        $(".clsCopperThickness").removeClass("active");
                        $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                        $("input[name='radCopperThickness']").val("1 oz Cu")

                    }


                    if (radInsideThickness == "3" || radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {


                        $(".clsInsideThickness").removeClass("active");
                        $(".clsInsideThickness[data='1']").addClass("active");
                        $("input[name='radInsideThickness']").val("1")
                    }
                }
                else {
                    if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 4) {
                        $(".clsCopperThickness").removeClass("active");
                        $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                        $("input[name='radCopperThickness']").val("1 oz Cu")

                    }


                    if (radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {


                        $(".clsInsideThickness").removeClass("active");
                        $(".clsInsideThickness[data='1']").addClass("active");
                        $("input[name='radInsideThickness']").val("1")
                    }
                }



            }
            else {
                $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn");

                $(".clsCopperThickness[data='7 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='8 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='9 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='10 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='11 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='12 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='13 oz Cu']").addClass("unclickbtn");


                $(".[data='5']").addClass("unclickbtn");
                $(".clsInsideThickness[data='6']").addClass("unclickbtn");

                if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 5) {
                    $(".clsCopperThickness").removeClass("active");
                    $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                    $("input[name='radCopperThickness']").val("1 oz Cu")

                }

                if (radInsideThickness == "5" || radInsideThickness == "6") {


                    $(".clsInsideThickness").removeClass("active");
                    $(".clsInsideThickness[data='1']").addClass("active");
                    $("input[name='radInsideThickness']").val("1")
                }
            }

            $(".clsLineWeight[data='3/3mil']").addClass("unclickbtn").removeClass("active");
            if (radLineWeight == "3/3mil") {

                $(".clsLineWeight[data='8/8mil']").removeClass("unclickbtn").addClass("active");
                $("input[name='radLineWeight']").val("8/8mil");
                radLineWeight = "8/8mil";
            }
            else {

            }
        } else {

            $(".clsLineWeight[data='3/3mil']").removeClass("unclickbtn");


            if (radBoardThickness == 0.2) {
                $(".clsCopperThickness[data='2 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='7 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='8 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='9 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='10 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='11 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='12 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='13 oz Cu']").addClass("unclickbtn");



                $(".clsInsideThickness[data='2']").addClass("unclickbtn");
                $(".clsInsideThickness[data='3']").addClass("unclickbtn");
                $(".clsInsideThickness[data='4']").addClass("unclickbtn");
                $(".clsInsideThickness[data='5']").addClass("unclickbtn");
                $(".clsInsideThickness[data='6']").addClass("unclickbtn");



                if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 2) {
                    $(".clsCopperThickness").removeClass("active");
                    $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                    $("input[name='radCopperThickness']").val("1 oz Cu")

                }


                if (radInsideThickness == "2" || radInsideThickness == "3" || radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {


                    $(".clsInsideThickness").removeClass("active");
                    $(".clsInsideThickness[data='1']").addClass("active");
                    $("input[name='radInsideThickness']").val("1")
                }

            }
            else if (radBoardThickness == 0.4) {

                if (radLineWeight != "8/8mil") {
                    $(".clsCopperThickness[data='2 oz Cu']").addClass("unclickbtn");
                    $(".clsInsideThickness[data='2']").addClass("unclickbtn");
                }

                $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='7 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='8 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='9 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='10 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='11 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='12 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='13 oz Cu']").addClass("unclickbtn");



                $(".clsInsideThickness[data='3']").addClass("unclickbtn");
                $(".clsInsideThickness[data='4']").addClass("unclickbtn");
                $(".clsInsideThickness[data='5']").addClass("unclickbtn");
                $(".clsInsideThickness[data='6']").addClass("unclickbtn");

                if (radLineWeight != "8/8mil") {
                    if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 2) {
                        $(".clsCopperThickness").removeClass("active");
                        $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                        $("input[name='radCopperThickness']").val("1 oz Cu")

                    }


                    if (radInsideThickness == "2" || radInsideThickness == "3" || radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {


                        $(".clsInsideThickness").removeClass("active");
                        $(".clsInsideThickness[data='1']").addClass("active");
                        $("input[name='radInsideThickness']").val("1")
                    }
                }
                else {
                    if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 3) {
                        $(".clsCopperThickness").removeClass("active");
                        $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                        $("input[name='radCopperThickness']").val("1 oz Cu")

                    }


                    if (radInsideThickness == "3" || radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {


                        $(".clsInsideThickness").removeClass("active");
                        $(".clsInsideThickness[data='1']").addClass("active");
                        $("input[name='radInsideThickness']").val("1")
                    }
                }



            }
            else if (radBoardThickness == 0.6 || radBoardThickness == 0.8) {

                if (radLineWeight != "8/8mil") {
                    $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn");
                    $(".clsInsideThickness[data='3']").addClass("unclickbtn");
                }


                $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='7 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='8 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='9 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='10 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='11 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='12 oz Cu']").addClass("unclickbtn");
                $(".clsCopperThickness[data='13 oz Cu']").addClass("unclickbtn");




                $(".clsInsideThickness[data='4']").addClass("unclickbtn");
                $(".clsInsideThickness[data='5']").addClass("unclickbtn");
                $(".clsInsideThickness[data='6']").addClass("unclickbtn");

                if (radLineWeight != "8/8mil") {
                    if (parseFloat(radCopperThickness.replace("oz Cu", "")) >=3) {
                        $(".clsCopperThickness").removeClass("active");
                        $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                        $("input[name='radCopperThickness']").val("1 oz Cu")

                    }


                    if (radInsideThickness == "3" || radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {


                        $(".clsInsideThickness").removeClass("active");
                        $(".clsInsideThickness[data='1']").addClass("active");
                        $("input[name='radInsideThickness']").val("1")
                    }
                }
                else {
                    if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 4) {
                        $(".clsCopperThickness").removeClass("active");
                        $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                        $("input[name='radCopperThickness']").val("1 oz Cu")

                    }


                    if (radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {


                        $(".clsInsideThickness").removeClass("active");
                        $(".clsInsideThickness[data='1']").addClass("active");
                        $("input[name='radInsideThickness']").val("1")
                    }
                }



            }
           

            if (radBoardThickness < 1.0) {

               $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn");
               $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn");
               $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn");

               $(".clsInsideThickness[data='4']").addClass("unclickbtn");
               $(".clsInsideThickness[data='5']").addClass("unclickbtn");
               $(".clsInsideThickness[data='6']").addClass("unclickbtn");

               if (radCopperThickness == "4 oz Cu" || radCopperThickness == "5 oz Cu" || radCopperThickness == "6 oz Cu") {
                   $(".clsCopperThickness").removeClass("active");
                   $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                   $("input[name='radCopperThickness']").val("1 oz Cu")

               }


               if (radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {


                   $(".clsInsideThickness").removeClass("active");
                   $(".clsInsideThickness[data='1']").addClass("active");
                   $("input[name='radInsideThickness']").val("1")
               }
            }

        }

        radInsideThickness = $("input[name='radInsideThickness']").val();
        radCopperThickness = $("input[name='radCopperThickness']").val();

        if (radLineWeight == "3/3mil" || radLineWeight == "4/4mil" || radLineWeight == "5/5mil") {
            $(".clsCopperThickness[data='2 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='7 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='8 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='9 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='10 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='11 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='12 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='13 oz Cu']").addClass("unclickbtn").removeClass("active");


            $(".clsInsideThickness[data='2']").addClass("unclickbtn").removeClass("active");
            $(".clsInsideThickness[data='3']").addClass("unclickbtn").removeClass("active");
            $(".clsInsideThickness[data='4']").addClass("unclickbtn").removeClass("active");
            $(".clsInsideThickness[data='5']").addClass("unclickbtn").removeClass("active");
            $(".clsInsideThickness[data='6']").addClass("unclickbtn").removeClass("active");

            if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 2) {
                $(".clsCopperThickness").removeClass("active");
                $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                $("input[name='radCopperThickness']").val("1 oz Cu")

            }


            if (radInsideThickness == "2" || radInsideThickness == "3" || radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {


                $(".clsInsideThickness").removeClass("active");
                $(".clsInsideThickness[data='1']").addClass("active");
                $("input[name='radInsideThickness']").val("1")
            }
        }
        else if (radLineWeight == "6/6mil") {

            $(".clsCopperThickness[data='3 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='4 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='7 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='8 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='9 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='10 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='11 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='12 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='13 oz Cu']").addClass("unclickbtn").removeClass("active");


            $(".clsInsideThickness[data='3']").addClass("unclickbtn").removeClass("active");
            $(".clsInsideThickness[data='4']").addClass("unclickbtn").removeClass("active");
            $(".clsInsideThickness[data='5']").addClass("unclickbtn").removeClass("active");
            $(".clsInsideThickness[data='6']").addClass("unclickbtn").removeClass("active");

            if (parseFloat(radCopperThickness.replace("oz Cu","")) >= 3) {
                $(".clsCopperThickness").removeClass("active");
                $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                $("input[name='radCopperThickness']").val("1 oz Cu")


            }



            if (radInsideThickness == "3" || radInsideThickness == "4" || radInsideThickness == "5" || radInsideThickness == "6") {

                $(".clsInsideThickness").removeClass("active");
                $(".clsInsideThickness[data='1']").addClass("active");
                $("input[name='radInsideThickness']").val("1")
            }
        } else if (radLineWeight == "8/8mil") {

            if (FR4Type == "Aluminum board") {
                $(".clsCopperThickness[data='5 oz Cu']").addClass("unclickbtn").removeClass("active");
                $(".clsCopperThickness[data='6 oz Cu']").addClass("unclickbtn").removeClass("active");
                $(".clsCopperThickness[data='7 oz Cu']").addClass("unclickbtn").removeClass("active");
                $(".clsCopperThickness[data='8 oz Cu']").addClass("unclickbtn").removeClass("active");
                $(".clsCopperThickness[data='9 oz Cu']").addClass("unclickbtn").removeClass("active");
                $(".clsCopperThickness[data='10 oz Cu']").addClass("unclickbtn").removeClass("active");
                $(".clsCopperThickness[data='11 oz Cu']").addClass("unclickbtn").removeClass("active");
                $(".clsCopperThickness[data='12 oz Cu']").addClass("unclickbtn").removeClass("active");
                $(".clsCopperThickness[data='13 oz Cu']").addClass("unclickbtn").removeClass("active");

                


                $(".clsInsideThickness[data='5']").addClass("unclickbtn").removeClass("active");
                $(".clsInsideThickness[data='6']").addClass("unclickbtn").removeClass("active");

                if (parseFloat(radCopperThickness.replace("oz Cu", "")) >=5) {
                    $(".clsCopperThickness").removeClass("active");
                    $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                    $("input[name='radCopperThickness']").val("1 oz Cu")

                }


                if (radInsideThickness == "5" || radInsideThickness == "6") {

                    $(".clsInsideThickness").removeClass("active");
                    $(".clsInsideThickness[data='1']").addClass("active");
                    $("input[name='radInsideThickness']").val("1")
                }

            } else {
                // $(".clsCopperThickness").removeClass("unclickbtn");
                //$(".clsInsideThickness").removeClass("unclickbtn");

            }


        }

       
        if (hidLayers < 4) {
            $(".clsMaterial[data='4']").addClass("unclickbtn");

            if ($("#FR4Type").val() == "HDI") {
                $("#FR4Type").val("FR-4");
                $(".clsMaterial[data='1']").addClass("active");
                $(".clsMaterial[data='4']").removeClass("active");

            }
        } else {
            $(".clsMaterial[data='4']").removeClass("unclickbtn");




            $(".clsCopperThickness[data='7 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='8 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='9 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='10 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='11 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='12 oz Cu']").addClass("unclickbtn").removeClass("active");
            $(".clsCopperThickness[data='13 oz Cu']").addClass("unclickbtn").removeClass("active");


          

            if (parseFloat(radCopperThickness.replace("oz Cu", "")) >= 7) {
                $(".clsCopperThickness").removeClass("active");
                $(".clsCopperThickness[data='1 oz Cu']").addClass("active");
                $("input[name='radCopperThickness']").val("1 oz Cu")

            }
        }
    }
}

function PCBCheck() {

    if ($("#dvBoardType:visible").size() > 0) {
        //if ($(":radio[name='radBoardType'][checked]").val() == "Panel PCB by PCBWay") {
        //    if (!$("#layoutx").validatebox("isValid")) {
        //        $("#layoutx").focus();
        //        return false;

        //    }

        //    if (!$("#layouty").validatebox("isValid")) {
        //        $("#layouty").focus();
        //        return false;

        //    }
        //}


        var val = $("#radBoardType").val();
        if (val != "Single PCB") {
            //if (!$("#iptCusomFirst").validatebox("isValid")) {
            //    $("#iptCusomFirst").focus();
            //    return false;

            //}
        }


        if ($("#radBoardType").val() != "Single PCB") {

            if (!$("#xoutyes").validatebox("isValid")) {
                $("#xoutyes").focus();
                return false;

            }



            //if ($(":radio[name='radAcceptCrossed']:checked").size() == 0) {

            //    alert("Please choose accept x-out in panel ?");
            //    $(":radio[name='radAcceptCrossed']").eq(0).focus();
            //    return false;
            //}
        }
    }


    if (!$("#hidLength").validatebox("isValid")) {
        $("#hidLength").focus();
        return false;

    }

    if (!$("#hidWidth").validatebox("isValid")) {
        $("#hidWidth").focus();
        return false;

    }


    if (!$("#txtPinBanNum").validatebox("isValid")) {
        $("#txtPinBanNum").focus();
        return false;

    }

    if (!$("#hidNum").validatebox("isValid")) {
        $("#hidNum").focus();
        return false;

    }


    var x = parseInt($("#hidLength").val());
    var y = parseInt($("#hidWidth").val());
    var num = parseInt($("#hidNum").val());


    if (isNaN(x) || x < 4) {
        alert($("#JsPlsInputLen").val());
        $("#hidLength").focus();
        return false;
    }


    if (isNaN(y) || y < 5) {
        alert($("#JsPlsInputWidth").val());
        $("#hidWidth").focus();
        return false;
    }

    if($("input[name='radGoldfingers']").val()=="Yes" && (x<4||y<4))
    {
       alert($("#JProductTip5").val());

       if(x<20)
       {
           $("#hidLength").focus();
           return false;
       }
       else
       {
           $("#hidWidth").focus();
           return false;
       }

       
    }


    if (y > 500 ) {
        alert($("#JsMaxLen").val());
        $("#hidWidth").focus();
        return false;
    }

    if (x > 400 ) {
        alert($("#JsMaxWidth").val());
        $("#hidLength").focus();
        return false;
    }
    if (isNaN(num) || num <= 4) {
        alert($("#JsPlsSelQty").val());
        $("#hidNum").focus();
        return false;
    }


    if ($(".custom-weight-show:visible").size() > 0) {//

        if (!$("#radAcceptHASLUp").validatebox("isValid")) {
            $("#radAcceptHASLUp").focus();
            return false;
        }

        //alert('Please choose accept we MIGHT Change "HASL" to "ENIG" ?');
        //$(":radio[name='radAcceptHASLUp']").eq(0).focus();    return false;


    }

    return true;
}




function checkForm() {

    if (online.PageType == 1 || online.PageType == 3) {

        if (online.PageType == 3) {

            if (!$("#txtBoardNum").validatebox("isValid")) {
                $("#txtBoardNum").focus();
                return false;
            }
            if ($("#dvSMTPCBRelation:visible").size() > 0) {//

                if (!$(".clsPCBRelation").hasClass("active")) {
                    $('#PCBRelationTip').show();
                    return false;
                }
                
            }



            if ($("input[name=chkPCB]")[0].checked == true) {
                if (PCBCheck() == false) {
                    return false;
                }
            }

        }
        else {


            if (PCBCheck() == false) {
                return false;
            }


            if ($("input[name=chkSMDStencil]")[0].checked == true) {
                if (!$("#iptStencilNum").validatebox("isValid")) {
                    $("#iptStencilNum").focus();
                    return false;
                }

            }

            if ($("input[name=chkSMT]")[0].checked == true) {
                if (!$("#txtBoardNum").validatebox("isValid")) {
                    $("#txtBoardNum").focus();
                    return false;
                }

            }

        }






    }
    else if (online.PageType == 4) {

        if (!$("#hidLength").validatebox("isValid")) {
            $("#hidLength").focus();
            return false;

        }

        if (!$("#hidWidth").validatebox("isValid")) {
            $("#hidWidth").focus();
            return false;

        }



        if (!$("#hidNum").validatebox("isValid")) {
            $("#hidNum").focus();
            return false;

        }


        var radGoldfingers = $("input[name='radGoldfingers']").val();
        //if (radGoldfingers == "Yes") {

        //    if (!$("input[name='radStiffMetalLength']").validatebox("isValid")) {
        //        $("input[name='radStiffMetalLength']").focus();
        //        return false;

        //    }
        //}

        //if ($("input[name='radStiffener']").val() != "Without") {

        //    if (!$("input[name='radPILength']").validatebox("isValid")) {
        //        $("input[name='radPILength']").focus();
        //        return false;

        //    }


        //    if (!$("input[name='radFrLength']").validatebox("isValid")) {
        //        $("input[name='radFrLength']").focus();
        //        return false;

        //    }

        //    if (!$("input[name='radStiffMetalLength']").validatebox("isValid")) {
        //        $("input[name='radStiffMetalLength']").focus();
        //        return false;

        //    }
        //}

        if ($("input[name=chkSMDStencil]")[0].checked == true) {
            if (!$("#iptStencilNum").validatebox("isValid")) {
                $("#iptStencilNum").focus();
                return false;
            }

        }

        if ($("input[name=chkSMT]")[0].checked == true) {
            if (!$("#txtBoardNum").validatebox("isValid")) {
                $("#txtBoardNum").focus();
                return false;
            }

        }

    }
    else {
        if (!$("#iptStencilNum").validatebox("isValid")) {
            $("#iptStencilNum").focus();
            return false;
        }
    }



    var txtPCBNote = $('#txtPCBNote').val();
    var txtSmtNote = $("#txtSmtNote").val();
    var txtStencilNote = $('#txtStencilNote').val();

    if (txtPCBNote && txtPCBNote.length > 600) {
        alert($('#wordsisover').val());
        $("#txtPCBNote").focus();
        return false;

    }

    if (txtSmtNote && txtSmtNote.length > 600) {
        alert($('#wordsisover').val());
        $("#txtSmtNote").focus();
        return false;

    }
    if (txtStencilNote && txtStencilNote.length > 600) {
        alert($('#wordsisover').val());
        $("#txtStencilNote").focus();
        return false;

    }





    return true;

}



function Step1(event) {



    //debugger;
    var e = event || window.event
    if (e) {
        if (e.stopPropagation) {
            e.stopPropagation();
        }
        else {
            e.cancelBubble = true;
        }
    }

    var spanPCBNos = $("#spanPCBNos").text();

    if (checkForm() == false) {
        return;
    }


    $("#dvmauiowright").show(100);
    $("#dvmauiowrightTwo").hide();

    $("#dvLoading").show();
    
    ShipChange();
    //setInfo();

}


function SelectAuNiOption()
{
    var iptHardGoldType = $("#iptHardGoldType").val();
    var HardGoldVal = "";
    if (iptHardGoldType == 1) {
         HardGoldVal = $("input[name='radAUGoldThickness']").val() + "," + $("input[name='radNiGoldThickness']").val();
    } else {
         HardGoldVal = $("input[name='radSendAUGoldThickness']").val() + "," + $("input[name='radSendNiGoldThickness']").val();
    }
  


    $(".clsHardGold").removeClass("active");

    var obj = $(".clsHardGold[data='" + HardGoldVal + "']");
    if (obj.length > 0) {
        $(".clsHardGold[data='" + HardGoldVal + "']").addClass("active");

        $("#iptAUVal").val('');
        $("#iptNIVal").val('');

    } else {
        $(".clsHardGold[data='customValHardGold']").addClass("active");
        $("#iptAUVal").val(HardGoldVal.split(',')[0]);
        $("#iptNIVal").val(HardGoldVal.split(',')[1]);

    }


    $('#dvHardGold').window('open'); // open a window
    $('#dvHardGold').window('center');
    $('#dvHardGold').show();
}

function SelectAuNiPdOption() {
    var iptENEPIGType = $("#iptENEPIGType").val();
    var ENEPIGVal = "";
    if (iptENEPIGType == 1) {
        ENEPIGVal = $("input[name='radPdNiGoldThickness']").val() + "," + $("input[name='radPdPdGoldThickness']").val() + "," + $("input[name='radPdAUGoldThickness']").val();
    } else {
        ENEPIGVal = $("input[name='radSendPdNiGoldThickness']").val() + "," + $("input[name='radSendPdPdGoldThickness']").val() + "," + $("input[name='radSendPdAUGoldThickness']").val();
    }



    $(".clsENEPIG").removeClass("active");

    var obj = $(".clsENEPIG[data='" + ENEPIGVal + "']");
    if (obj.length > 0) {
        $(".clsENEPIG[data='" + ENEPIGVal + "']").addClass("active");

        $("#iptPdAUVal").val('');
        //$("#iptPdNIVal").val('');
        $("#iptPdPdVal").val('');


    } else {
        $(".clsENEPIG[data='customValHardGold']").addClass("active");
        $("#iptPdAUVal").val(ENEPIGVal.split(',')[2]);
        // $("#iptPdNIVal").val(ENEPIGVal.split(',')[0]);
        $("#iptPdPdVal").val(ENEPIGVal.split(',')[1]);


    }


    $('#dvENEPIG').window('open'); // open a window
    $('#dvENEPIG').window('center');
    $('#dvENEPIG').show();
}

function clkBuildtime(obj) {
    $(obj).next().click();
}

var isSubmit = false;
function formSubmit(event) {
    //debugger;
    if (isSubmit == true) {
        return;
    }

    var e = event || window.event
    if (e) {
        if (e.stopPropagation) {
            e.stopPropagation();
        }
        else {
            e.cancelBubble = true;
        }
    }

    if (checkForm() == false)
        return false;

    var logFlag = $("#logFlag").val();
    if (logFlag == 0) {
        if (AddToBasket() == false) {
            return false;
        }
    }

    var url = "";

    // 向member域提交    
    memberHost = $("#JsMemberHost").val();

    if ($("#pagetype").val() == 1) {
        url = memberHost + "/Order/addtocart/"
    }
    else if ($("#pagetype").val() == 2) {
        //stencil
        url = memberHost + "/order/AddToCartStencil/";
    }
    else if ($("#pagetype").val() == 3) {
        //stm
        url = memberHost + "/order/AddToCartSMT/";
    }
    else if ($("#pagetype").val() == 4) {
        url = memberHost + "/order/addtocart/";
    }
    else {
        url = memberHost + "/order/addtocart/";
    }

    var txtPCBNote = $('#txtPCBNote').val();
    var txtSmtNote = $("#txtSmtNote").val();
    var txtStencilNote = $('#txtStencilNote').val();

    if (txtPCBNote && txtPCBNote.length > 600) {
        alert($('#wordsisover').val());
        $("#txtPCBNote").focus();
        return false;

    }

    if (txtSmtNote && txtSmtNote.length > 600) {
        alert($('#wordsisover').val());
        $("#txtSmtNote").focus();
        return false;

    }
    if (txtStencilNote && txtStencilNote.length > 600) {
        alert($('#wordsisover').val());
        $("#txtStencilNote").focus();
        return false;

    }

    $("#orderForm").attr("action", url);
    $("#orderForm").submit();
    isSubmit = true;
}

var selectTmpMoney = 0;

function setInfo() {
    var x = parseInt($("#hidLength").val());
    var y = parseInt($("#hidWidth").val());
    var num = parseInt($.trim($("#hidNum").val()));
    var Thickness = $(":radio[name='radBoardThickness']:checked").val();
    var Thickness = $("input[name='radBoardThickness']").val();
    var CopperWeight = $("input[name='radCopperThickness']").val();
    var SurfaceFinish = $("input[name='radPlatingType']").val();
    var CopperWeight = $(":radio[name='radCopperThickness']:checked").val();
    var SurfaceFinish = $(":radio[name='radPlatingType']:checked").val();
    var layers = $(":radio[name=hidLayers]:checked").val();
    var layers = $("input[name=hidLayers]").val();

    if (isNaN(x)) {
       $("#spRx").html(x);
    } else {
       $("#spRx").html(x + " (mm)");
    }
    if (isNaN(y)) {
       $("#spRy").html(y);
    } else {
       $("#spRy").html(y + " (mm)");
    }

    if (isNaN(Thickness)) {
       Thickness = ""
    } else {
       Thickness = Thickness + " mm"
    }

    if (isNaN(CopperWeight)) {
       CopperWeight = "";
    } else {
       CopperWeight = CopperWeight;
    }
    if (isNaN(num)) {
       num = ""
    } else {
       num = num + "  Pieces"
    }
    var hidSMTtotal = $("#hidSMTtotal").val() ? parseFloat($("#hidSMTtotal").val()) : 0;
    var hidShipMoney = $("#hidShipMoney").val() ? parseFloat($("#hidShipMoney").val()) : 0;
    var hidStenciltotal = $("#hidStenciltotal").val() ? parseFloat($("#hidStenciltotal").val()) : 0;



    var hidmoney = hidSMTtotal + parseFloat($("#hidMoney").val() ? $("#hidMoney").val() : "0") + hidShipMoney + hidStenciltotal;

    if (isNaN(hidmoney)) {
        hidmoney = parseFloat($("#hidMoney").val() ? $("#hidMoney").val() : "0") + hidStenciltotal + hidSMTtotal;
    }
    if (isNaN(hidmoney)) {
        hidmoney = ""
    } else {
        hidmoney = hidmoney;
    }

    //$("#spRnum").html(num);
    // $("#spRmoney").html("$" + hidmoney);
    // $("#spRday").html($("#hidDay").val());
    // $("#spRnote").html("" + layers + " layers  " + Thickness + " " + SurfaceFinish + " " + CopperWeight + "");
    $("#assembly-total").html(hidmoney);
    //$("#tippic").attr("src", "/images/3drenders/" + layers + "layer.jpg");
    // $("#spMTotal").html("$" + $("#hidMoney").val());

    var disbasemoney = hidSMTtotal + parseFloat($("#hidMoney").val() ? $("#hidMoney").val() : "0") + hidStenciltotal;
    if (disbasemoney > 0) {
        $.post("/Quote/GetMbDiscount/", { disBaseMoney: disbasemoney, userNo: $("#hiduserNo").val() }, function (d) {
            if (d.result) {
                $("#isShowDiscount").show();
                //$("#spLevel").html(d.msg.Data.level);
                $("#spRate").html(d.msg.Data.rate);
                $("#showDiscount").html(d.msg.Data.discount);

                if ($("#assembly-total").html() != "") {
                    $("#assembly-total").html(parseFloat(hidmoney) - d.msg.Data.discount);
                }
            }
            else {
                $("#isShowDiscount").hide();
            }

            //if ($("#FR4Type").val() == "Rogers") {
            //    $("#spMTotal").html("?");

            //    $("#assembly-total").html("?");
            //}
        })
    }
    else {
        $("#isShowDiscount").hide();
    }
    $("#spMTotal").html($("#hidMoney").val());
    //$("#spSaveMoney").html($("#hidWebSaveMoney").val());

    //if ($("#FR4Type").val() == "Rogers") {
    //    $("#spMTotal").html("?");
    //    $("#assembly-total").html("?");
    //}

}


function ChooseDeliverySMT(o, p, discount, original) {
    $("#SMTtotal").text(p);
    p = p - discount;
    //
    var e = window.event
    if (e) {
        if (e.stopPropagation) {
            e.stopPropagation();
        }
        else {
            e.cancelBubble = true;
        }
    }

    if (original && parseFloat(original) > parseFloat(p)) {
        $("#dvdelcoupon").show().text(original);
    }
    else {

        $("#dvdelcoupon").hide();
    }


    $(".clsChoDeliverySMT").attr("class", "clsChoDeliverySMT option clearfix");
    $(o).addClass("selected");
    // $("#assembly-total").html(p);
    $("#hidSMTtotal").val(p);

    $("#hidDaySMT").val($(o).find(".clsHidDay").attr("data"));
    $("#hidDeliveryDaysSMT").val($(o).find("input").val());

    $("#hidSMTQty").val($(o).find(".qty-div").text());

    if (discount > 0) {
        $("#dvSaveSMT").show();
        $("#spSaveSMT").text(discount);

    }
    else {
        $("#dvSaveSMT").hide();
        $("#spSaveSMT").text(0);

    }
    // $("#hidWebSaveMoney").val((parseFloat(p1) - parseFloat(p)));
    selectTmpMoney = p;
    setInfo();
}


function ChooseDelivery(o, p, isjiaji, data) {
    //
    var e = window.event
    if (e) {
        if (e.stopPropagation) {
            e.stopPropagation();
        }
        else {
            e.cancelBubble = true;
        }
    }


    $(".clsChoDelivery").attr("class", "clsChoDelivery option clearfix");
    $(o).addClass("selected");
    // $("#assembly-total").html(p);
    $("#hidMoney").val(p);
    $("#hidDay").val($(o).find(".clsHidDay").attr("data"));
    $("#hidDeliveryDays").val($(o).find("input").val());

    //计算最长到货时间2018-3-7
    var st = parseInt($(o).find("input").val());
    var nowhour = parseInt($('#nowhour').val());
    if ($(o).find("input").val() == 12) {

        $("#tip_pay1").hide();
        $("#tip_pay2").show();

        if (nowhour >= 0 && nowhour < 17) {
            $('#timetype').text("AM");
            st = 24;
        }
        else {
            $('#timetype').text("AM");
            st = 48;

        }

    }
    else {

        $("#tip_pay2").hide();
        $("#tip_pay1").show();

        if (isjiaji) {


            if (nowhour >= 6 && nowhour < 18) {
                $('#timetype').text("AM");
            }
            else {
                $('#timetype').text("PM");
            }

        }
        else {
            $('#timetype').text("AM");
        }
    }


    st = st / 24;
    if (st == 2 && data.indexOf('1-2') > -1)
        st = 1;


    var stratbuilddate = new Date($('#stratbuilddate').attr('data-date'));

    if ($(o).find("input").val() == 12) {
        stratbuilddate = new Date($('#iptnowdate').val());
    }
    else {

    }


    stratbuilddate.setDate(stratbuilddate.getDate() + st);

    //if (stratbuilddate.getDay() == 0)
    //{
    //    stratbuilddate.setDate(stratbuilddate.getDate() + 1);
    //}


    $('#stratbuilddate').html(stratbuilddate.getFullYear() + '/' + (stratbuilddate.getMonth() + 1) + '/' + stratbuilddate.getDate());
    var st = Math.ceil((parseInt($("#spShipDays").attr('data-days').split('-')[0]) + parseInt($("#spShipDays").attr('data-days').split('-')[1])) / 2);

    var shipdate = new Date($('#stratbuilddate').html());
    shipdate.setDate(shipdate.getDate() + st);
    $('#shipdate').html(shipdate.getFullYear() + '/' + (shipdate.getMonth() + 1) + '/' + shipdate.getDate());

    // $("#hidWebSaveMoney").val((parseFloat(p1) - parseFloat(p)));
    selectTmpMoney = p;
    setInfo();
}

function ShipCountryChange(cid, pic, cname) {

    $('.list-title').click();

    if (pic > 0) {
        $("#imgShipCountryPic").show().attr("src", "/img/images/country/" + pic + ".gif");
        $('.list-title').find('img').show().attr('src', '/img/images/country/' + pic + '.gif');
    }
    else {
        $("#imgShipCountryPic").hide();
        $('.list-title').find('img').hide();
    }

    $('#selShipCountry').val(cid);



    $('.list-title').find('span').html(cname);
    //var cid = $("#selShipCountry").val();

    $.getJSON("/Order/GetShipType", { cid: cid }, function (d) {
        if (d.ok) {
            $("#selShip").html("");
            $("#selShip").append('<option pic="" ids="" value="0">' + $("#JsChooseShip").val() + '</option>');
            $.each(d.data, function (i, item) {
                if (cid == 174) {
                    $("#selShip").append('<option pic="' + item.Pic + '" value="' + item.Id + '"' + ("EMS" == item.ClassName ? " selected" : "") + '>' + item.ClassName + '</option>');
                } else {
                    $("#selShip").append('<option pic="' + item.Pic + '" value="' + item.Id + '"' + ("DHL" == item.ClassName ? " selected" : "") + '>' + item.ClassName + '</option>');
                }
            });
            ShipChange();
        }
        else {
            alert($("#JsErrGetShipping").val());
        }
    })

}

function AddToBasket() {

    //var sid = $("#selShip").val();
    //var cid = $("#selShipCountry").val();

    //if (sid == 0) {
    //    alert("Please choose Shipping costs.");
    //    $("#selShip").focus();
    //    return false;
    //}
    var logFlag = $("#logFlag").val();
    //未登录
    if (logFlag == 0) {
        var mail = $.trim($("#txtMail").val());
        if (mail.length == 0) {
            layer.alert($("#JsPlsInputEmail").val(), { icon: 0, btn: 'OK', title: 'Information' }, function (index) { $("#txtMail").focus(); layer.close(index); });

            return false;
        }


        var patn = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        if (!patn.test(mail)) {

            layer.alert($("#JsPlsEnterValidEmail").val(), { icon: 0, btn: 'OK', title: 'Information' }, function (index) { $("#txtMail").focus(); layer.close(index); });

            return false;

        }
    }
    //$("#step3Div").removeClass("active").find(".content").hide();
    //$("#step4Div").show().addClass("active").find(".content").show(); ;

    //var s = $("#orderForm").serialize();
    //$.post("/member/ajax/ajaxorder.aspx?act=saveguid", s, function (d) {
    //    /*alert(d);
    //    $("#hidTmpNO").val(d);
    //    if (d.toString().length > 2) {
    //    $("#orderForm").submit();
    //    }*/
    //    if (d != "1") {
    //        alert(d);
    //        return false;
    //    }
    //    alert("Please join us or sign in then finish order!")
    //    window.location = "/member/login.aspx?loginEmail=" + mail + "&returnurl=%2fmember%2forder%2forderview.aspx%3fshowUpload%3dtrue%26isstat%3dy";
    //    return false;
    //});

}

var selectedcountryid = 0
function ShipChange() {

    if ($("#selShipCountry").val() && parseInt($("#selShipCountry").val()) > 0) {
        selectedcountryid = $("#selShipCountry").val();
    }


    var shippic = $('#selShip').find("option:selected").attr("pic");
    var sid = $("#selShip").val();
    if (sid == 2 || !shippic || shippic.length == 0)
        $("#imgShipPic").hide();
    else {
        $("#imgShipPic").show();

        $("#imgShipPic").attr("src", "/img/images/country/" + $('#selShip').find("option:selected").attr("pic"));

    }

    var x = $("#hidLength").val();
    var y = $("#hidWidth").val();
    var num = $("#hidNum").val();
    var Thickness = $("input[name='radBoardThickness']").val();
    var chkSMDStencil = $("input[name='chkSMDStencil']").is(':checked') ? "on" : "";
    var shipname = $.trim($('#selShip option:selected').text());
    var countryname = $.trim($('#selShipCountry option:selected').text());

    var cid = $("#selShipCountry").val();
    var chkPCB = $("input[name='chkPCB']").is(':checked') ? "on" : "";
    var chkSMT = $("input[name='chkSMT']").is(':checked') ? "on" : "";

    var FR4Type = $('#FR4Type').val();



    if (sid == 2) {
        $("#spShipTotal").html("?");
        $("#spShipMoeny").html("?");
        $("#hidShipMoney").val(0);
        $("#hidShipName").val(shipname);
        // $("#hidCountryName").val(countryname);
        //$("#hidCountryId").val(cid);
        $("#spShipDays").html($("#JsContactUsForCheaper").val());
        setInfo();
    }
}
