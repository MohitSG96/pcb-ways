from django.http import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from .models import Selection,Personaldetails
from .serializers import SelectionSerializer,UserSerializer
from rest_framework import viewsets
from rest_framework.decorators import action
# from rest_framework.decorators import action
# from django.core.mail import send_mail
#
# from rest_framework.views import APIView
# from rest_framework.parsers import MultiPartParser, FormParser
# from rest_framework.response import Response
# from rest_framework import status


class SelectionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Selection.objects.all().order_by('-id')
    serializer_class = SelectionSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Personaldetails.objects.all().order_by('-id')
    serializer_class = UserSerializer
