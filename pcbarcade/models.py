from django.db import models
# Create your models here.


class Personaldetails(models.Model):
    email_address=models.EmailField(max_length=50)
    phone_number=models.CharField(max_length=25)
    full_address=models.TextField(default="")
    file=models.FileField()
    full_name = models.CharField(max_length=80, null=True)

    def __str__(self):
        return f'Email: {self.email_address}    Mobile: {self.phone_number}'

    class Meta:
        verbose_name = 'User Detail'


class Selection(models.Model):
    board_type= models.CharField(max_length=30,null=True)
    different_design = models.IntegerField(null=True)
    size = models.CharField(max_length=5,null=True)
    quality=models.IntegerField(null=True)
    layers = models.CharField(max_length=10,null=True)
    material = models.CharField(max_length=100,null=True)
    FR4_TG= models.CharField(max_length=25,blank=True,null=True)
    thickness=models.FloatField(max_length=10,null=True)
    min_track=models.CharField(max_length=10,null=True)
    min_hole_size=models.CharField(max_length=9,null=True)
    solder_mask=models.CharField(max_length=10,null=True)
    silkscreen=models.CharField(max_length=10,null=True)
    gold_fingers = models.CharField(max_length=10,null=True)
    surface_finish=models.CharField(max_length=30,null=True)
    thickness_of_immersion_gold=models.CharField(max_length=30,null=True)
    via_process=models.CharField(max_length=25,null=True)
    finished_copper=models.CharField(max_length=10,null=True)
    extra_pcb=models.BooleanField(default=False)
    other_special_request=models.TextField(null=True)
    ship_type=models.CharField(max_length=20,null=True)
    postal_code=models.CharField(max_length=20,null=True)
    city=models.CharField(max_length=30,null=True)
    country_code=models.CharField(max_length=10,null=True)
    length=models.FloatField(max_length=10,null=True)
    width=models.FloatField(max_length=10,null=True)
    castellated_holes = models.CharField(max_length=5, default="No")
    total_cost = models.FloatField(null=True)
    userId = models.ForeignKey(Personaldetails, on_delete=models.CASCADE)
    site = models.CharField(max_length=10, null=True)
    oder_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return f'Order Id:{self.pk}   User:{self.userId.full_name}'

    class Meta:
        verbose_name = 'User Order'

